﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace VirdiMiddleware
{
    public partial class mdiVirdi : Form
    {
        LogGenerator objLogGenerator;//Object of Loggenerator Class
        Thread thrLogfile;

        public mdiVirdi()
        {
            InitializeComponent();
            objLogGenerator = new LogGenerator();
        }

        private void tsmMiddleware_Click(object sender, EventArgs e)
        {
            try
            {
                frmMiddleware ObjAtt = new frmMiddleware();
                //if (checkForm(ObjAtt))
                {
                    ObjAtt.MdiParent = this;
                    ObjAtt.Show();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void tsmExit_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to close Virdi Middleware application?", "Confirm Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foreach (Form childForm in MdiChildren)
                    {
                        childForm.Close();
                    }
                    Application.Exit();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Boolean checkForm(Form f1)
        {
            Boolean b = true;
            foreach (Form frm in mdiVirdi.ActiveForm.MdiChildren)
            {
                if (frm.Name == f1.Name)
                {
                    frm.Focus();
                    b = false;
                    break;
                }
            }
            return b;
        }

        private void mdiVirdi_Load(object sender, EventArgs e)
        {
            try
            {
                //Crating LogFiles folder for log files if not
                if (!Directory.Exists(System.Windows.Forms.Application.StartupPath + "\\LogFiles\\"))
                    Directory.CreateDirectory(System.Windows.Forms.Application.StartupPath + "\\LogFiles\\");

                thrLogfile = new Thread(CreateLogFile);
                thrLogfile.IsBackground = true;
                thrLogfile.Start();

                objLogGenerator.CheckAndCreateTextFile();

                frmMiddleware ObjAtt = new frmMiddleware();
                ObjAtt.MdiParent = this;
                ObjAtt.Show();
            }
            catch (Exception ex)
            {
                objLogGenerator.WriteToLog("Error in tmrLogfile_Tick: " + ex.Message);
            }
        }

        //CreateLogFile FUNCTION FOR CHECKING AND CREATING LOG FILE FOR EACH DAY
        public void CreateLogFile()
        {
            try
            {
                while (true)
                {
                    Thread.Sleep(60000);

                    if (!File.Exists(System.Windows.Forms.Application.StartupPath + "\\LogFiles\\" + "Log-" + System.DateTime.Now.ToShortDateString().Replace("/", "-") + ".txt"))
                    {
                        objLogGenerator.CloseLog();
                        objLogGenerator.CheckAndCreateTextFile();
                    }
                }
            }
            catch (Exception ex)
            {
                objLogGenerator.WriteToLog("Error in tmrLogfile_Tick: " + ex.Message);
            }
        }
    }
}
