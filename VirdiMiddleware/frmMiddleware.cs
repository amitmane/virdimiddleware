﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using UCSAPICOMLib;
using UCBioBSPCOMLib;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using AttendanceID.Model.Tables;
using AttendanceID.Model.Views;
using AttendanceID.Model;

using System.IO;
using System.Net;
using System.Net.Sockets;
using Com.Rasilant.Services;

namespace VirdiMiddleware
{
    //Virdi Middleware Application
    public partial class frmMiddleware : Form
    {
        #region Diclaration

        LogGenerator objLogGenerator;//Object of Loggenerator Class
        GetSetting objGetSetting;//Object of Getsetting Class
        List<Terminal> TerminalList;

        Task tskConnectionStatus;
        Task tskPortReading;
        Task tskEnrolment;

        // UCSAPI
        public UCSAPICOMLib.UCSAPI ucsAPI;
        private IServerUserData serveruserData;
        private ITerminalUserData terminalUserData;

        private IServerAuthentication serverAuthentication;
        private IAccessLogData accessLogData;
        private ITerminalOption terminalOption;

        // UCBioBSP
        public UCBioBSPCOMLib.UCBioBSP ucBioBSP;
        public IFPData fpData;
        private ITemplateInfo templateInfo;
        public IDevice device;
        public IExtraction extraction;
        public IFastSearch fastSearch;
        public IMatching matching;

        // initialize valiables member
        public string szTextEnrolledFIR;

        public byte[] binaryEnrolledFIR;
        public string terminalID;
        public string userID;
        public readonly long nTemplateType400 = 400;
        public readonly long nTemplateType800 = 800;
        public readonly long nTemplateType320 = 320;
        public readonly long nTemplateType256 = 256;
        public string txtFilter;
        public string txtMessage;

        #endregion Diclaration

        #region Form Events

        public frmMiddleware()
        {
            try
            {
                InitializeComponent();
                objGetSetting = new GetSetting();
                objLogGenerator = new LogGenerator();
                TerminalList = new List<Terminal>();


            }
            catch (Exception ex)
            {
                objLogGenerator.WriteToLog("Error at frmMiddleware " + ex.Message);
            }
        }

        private void frmMiddleware_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                ucsAPI.ServerStop();
                objLogGenerator.WriteToLog("Server Stops. Code : " + ucsAPI.ErrorCode.ToString("X4"));
                objGetSetting.MiddWinFlag = false;
            }
            catch (Exception ex)
            {
                objLogGenerator.WriteToLog("Server Stops. Code : " + ucsAPI.ErrorCode.ToString("X4"));
                //objLogGenerator.WriteToLog("frmMiddleware_FormClosed " + ex.Message);
            }
        }

        private void frmMiddleware_Load(object sender, EventArgs e)
        {
            try
            {
                lbxMessage.Items.Insert(0, "Searching Virdi Terminals.");
                objLogGenerator.WriteToLog("Searching Virdi Terminals.");

                // create UCSAPI Instance
                ucsAPI = new UCSAPIClass();
                serveruserData = ucsAPI.ServerUserData as IServerUserData;
                terminalUserData = ucsAPI.TerminalUserData as ITerminalUserData;
                accessLogData = ucsAPI.AccessLogData as IAccessLogData;
                serverAuthentication = ucsAPI.ServerAuthentication as IServerAuthentication;
                terminalOption = ucsAPI.TerminalOption as ITerminalOption;


                // create UCBioBSP Instance
                ucBioBSP = new UCBioBSPClass();
                fpData = ucBioBSP.FPData as IFPData;
                device = this.ucBioBSP.Device as IDevice;
                extraction = this.ucBioBSP.Extraction as IExtraction;
                fastSearch = this.ucBioBSP.FastSearch as IFastSearch;
                matching = this.ucBioBSP.Matching as IMatching;

                // create event handle
                ucsAPI.EventTerminalConnected += new _DIUCSAPIEvents_EventTerminalConnectedEventHandler(UCSCOMObj_EventTerminalConnected);
                ucsAPI.EventTerminalDisconnected += new _DIUCSAPIEvents_EventTerminalDisconnectedEventHandler(UCSCOMObj_EventTerminalDisconnected);
                ucsAPI.EventAddUser += new _DIUCSAPIEvents_EventAddUserEventHandler(ucsAPI_EventAddUser);
                ucsAPI.EventAntipassback += new _DIUCSAPIEvents_EventAntipassbackEventHandler(ucsAPI_EventAntipassback);
                //ucsAPI.EventAuthTypeWithUniqueID += new _DIUCSAPIEvents_EventAuthTypeWithUniqueIDEventHandler(ucsAPI_EventAuthTypeWithUniqueID);
                //ucsAPI.EventAuthTypeWithUserID += new _DIUCSAPIEvents_EventAuthTypeWithUserIDEventHandler(ucsAPI_EventAuthTypeWithUserID);
                ucsAPI.EventControlPeripheralDevice += new _DIUCSAPIEvents_EventControlPeripheralDeviceEventHandler(ucsAPI_EventControlPeripheralDevice);
                ucsAPI.EventDeleteAllUser += new _DIUCSAPIEvents_EventDeleteAllUserEventHandler(ucsAPI_EventDeleteAllUser);
                ucsAPI.EventDeleteUser += new _DIUCSAPIEvents_EventDeleteUserEventHandler(ucsAPI_EventDeleteUser);
                ucsAPI.EventFingerImageData += new _DIUCSAPIEvents_EventFingerImageDataEventHandler(ucsAPI_EventFingerImageData);
                ucsAPI.EventFirmwareUpgraded += new _DIUCSAPIEvents_EventFirmwareUpgradedEventHandler(ucsAPI_EventFirmwareUpgraded);
                ucsAPI.EventFirmwareUpgrading += new _DIUCSAPIEvents_EventFirmwareUpgradingEventHandler(ucsAPI_EventFirmwareUpgrading);
                ucsAPI.EventFirmwareVersion += new _DIUCSAPIEvents_EventFirmwareVersionEventHandler(ucsAPI_EventFirmwareVersion);
                ucsAPI.EventGetAccessLog += new _DIUCSAPIEvents_EventGetAccessLogEventHandler(ucsAPI_EventGetAccessLog);
                ucsAPI.EventGetAccessLogCount += new _DIUCSAPIEvents_EventGetAccessLogCountEventHandler(ucsAPI_EventGetAccessLogCount);
                ucsAPI.EventGetTAFunction += new _DIUCSAPIEvents_EventGetTAFunctionEventHandler(ucsAPI_EventGetTAFunction);
                ucsAPI.EventGetUserCount += new _DIUCSAPIEvents_EventGetUserCountEventHandler(ucsAPI_EventGetUserCount);
                ucsAPI.EventGetUserData += new _DIUCSAPIEvents_EventGetUserDataEventHandler(ucsAPI_EventGetUserData);
                ucsAPI.EventGetUserInfoList += new _DIUCSAPIEvents_EventGetUserInfoListEventHandler(ucsAPI_EventGetUserInfoList);
                ucsAPI.EventOpenDoor += new _DIUCSAPIEvents_EventOpenDoorEventHandler(ucsAPI_EventOpenDoor);
                ucsAPI.EventPictureLog += new _DIUCSAPIEvents_EventPictureLogEventHandler(ucsAPI_EventPictureLog);
                ucsAPI.EventRealTimeAccessLog += new _DIUCSAPIEvents_EventRealTimeAccessLogEventHandler(ucsAPI_EventRealTimeAccessLog);
                ucsAPI.EventSetAccessControlData += new _DIUCSAPIEvents_EventSetAccessControlDataEventHandler(ucsAPI_EventSetAccessControlData);
                ucsAPI.EventSetTAFunction += new _DIUCSAPIEvents_EventSetTAFunctionEventHandler(ucsAPI_EventSetTAFunction);
                ucsAPI.EventSetTATime += new _DIUCSAPIEvents_EventSetTATimeEventHandler(ucsAPI_EventSetTATime);
                ucsAPI.EventTerminalStatus += new _DIUCSAPIEvents_EventTerminalStatusEventHandler(ucsAPI_EventTerminalStatus);
                //ucsAPI.EventVerifyCard += new _DIUCSAPIEvents_EventVerifyCardEventHandler(ucsAPI_EventVerifyCard);
                //ucsAPI.EventVerifyFinger1to1 += new _DIUCSAPIEvents_EventVerifyFinger1to1EventHandler(ucsAPI_EventVerifyFinger1to1);
                //ucsAPI.EventVerifyFinger1toN += new _DIUCSAPIEvents_EventVerifyFinger1toNEventHandler(ucsAPI_EventVerifyFinger1toN);
                ucsAPI.EventVerifyPassword += new _DIUCSAPIEvents_EventVerifyPasswordEventHandler(ucsAPI_EventVerifyPassword);
                ucsAPI.EventPrivateMessage += new _DIUCSAPIEvents_EventPrivateMessageEventHandler(ucsAPI_EventPrivateMessage);
                ucsAPI.EventPublicMessage += new _DIUCSAPIEvents_EventPublicMessageEventHandler(ucsAPI_EventPublicMessage);
                ucsAPI.EventUserFileUpgrading += new _DIUCSAPIEvents_EventUserFileUpgradingEventHandler(ucsAPI_EventUserFileUpgrading);
                ucsAPI.EventUserFileUpgraded += new _DIUCSAPIEvents_EventUserFileUpgradedEventHandler(ucsAPI_EventUserFileUpgraded);
                ucsAPI.EventEmergency += new _DIUCSAPIEvents_EventEmergencyEventHandler(ucsAPI_EventEmergency);
                ucsAPI.EventSetEmergency += new _DIUCSAPIEvents_EventSetEmergencyEventHandler(ucsAPI_EventSetEmergency);

                ucsAPI.EventTerminalControl += new _DIUCSAPIEvents_EventTerminalControlEventHandler(ucsAPI_EventTerminalControl);
                ucsAPI.EventRegistFace += new _DIUCSAPIEvents_EventRegistFaceEventHandler(ucsAPI_EventRegistFace);

                ucBioBSP.OnCaptureEvent += new _IUCBioBSPEvents_OnCaptureEventEventHandler(ucBioBSP_OnCaptureEvent);
                ucBioBSP.OnEnrollEvent += new _IUCBioBSPEvents_OnEnrollEventEventHandler(ucBioBSP_OnEnrollEvent);

                System.Windows.Forms.Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);

                ucsAPI.ServerStart(600, 9870);
                lbxMessage.Items.Insert(0, "Server Starts. Code : " + ucsAPI.ErrorCode.ToString("X4"));
                objLogGenerator.WriteToLog("Server Starts. Code : " + ucsAPI.ErrorCode.ToString("X4"));
                GC.Collect();

                //tskPortReading = new Task(() => ReadingServerPort());
                //tskPortReading.Start();


                tskEnrolment = new Task(() => AutoUserEnrolment());
                tskEnrolment.Start();

            }
            catch (Exception ex)
            {
                objLogGenerator.WriteToLog("Error at frmMiddleware_Load " + ex.Message);
            }
        }


        #endregion Form Events

        #region Functions

        void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            objLogGenerator.WriteToLog("Error at Application_ThreadException " + e.Exception.ToString());
        }

        private void GetUnReadSwipes(int TerminalID)
        {
            try
            {
                accessLogData.GetAccessLogCountFromTerminal(0, TerminalID, 0);//0 = New Swipes,1 = OLD, 2 = ALL
            }
            catch (Exception ex)
            {
                objLogGenerator.WriteToLog("Error at GetUnReadSwipes" + ex.Message);
            }
        }

        private void ConnectionStatus()
        {
            try
            {
                while (true)
                {
                    tskConnectionStatus.Wait(10000);

                    if (TerminalList.Count > 0)
                    {
                        var toDelete = TerminalList.FirstOrDefault(x => x.ConnectionTime < DateTime.Now.AddMinutes(-1));

                        if (toDelete != null)
                        {
                            TerminalList.Remove(toDelete);
                            lbxMessage.Invoke((Action)(() => lbxMessage.Items.Insert(0, System.DateTime.Now + ".-. " + "Terminal Disconnected TerminalID :" + toDelete.TerminalCode + " Time : " + toDelete.ConnectionTime + " " + TerminalList.Count)));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objLogGenerator.WriteToLog("ConnectionStatus() :  " + ex.Message);
            }
            finally
            {
                ConnectionStatus();
            }
        }


        #endregion Functions

        #region  envet handler process

        void UCSCOMObj_EventTerminalConnected(int TerminalID, string TerminalIP)
        {
            try
            {
                lbxMessage.Items.Insert(0, System.DateTime.Now + ".-." + "Terminal Connected TerminalID : " + TerminalID + " TerminalIP : " + TerminalIP + " Code : " + this.ucsAPI.ErrorCode);
                objLogGenerator.WriteToLog("EventTerminalConnected TerminalID : " + TerminalID + " TerminalIP : " + TerminalIP + " Code : " + this.ucsAPI.ErrorCode);

                Terminal objTerminal = new Terminal();
                objTerminal.TerminalCode = TerminalID;
                objTerminal.ConnectionTime = DateTime.Now;

                var toUpdate = TerminalList.SingleOrDefault(x => x.TerminalCode == TerminalID);
                if (toUpdate != null)
                    toUpdate.ConnectionTime = DateTime.Now;
                else
                    TerminalList.Add(objTerminal);

                GetUnReadSwipes(TerminalID);

                tskConnectionStatus = new Task(() => ConnectionStatus());
                tskConnectionStatus.Start();
            }
            catch (Exception ex)
            {
                objLogGenerator.WriteToLog("UCSCOMObj_EventTerminalConnected " + ex.Message);
            }
        }

        void ucsAPI_EventFirmwareVersion(int ClientID, int TerminalID, string Version)
        {
            try
            {
                lbxMessage.Items.Insert(0, System.DateTime.Now + ".-." + "Terminal - ClientID : " + ClientID + " TerminalID : " + TerminalID + " Version : " + Version + " Code : " + this.ucsAPI.ErrorCode);
                objLogGenerator.WriteToLog("EventFirmwareVersion  - ClientID : " + ClientID + " TerminalID : " + ClientID + TerminalID + " Version : " + Version + " Code : " + this.ucsAPI.ErrorCode);
            }
            catch (Exception ex)
            {
                objLogGenerator.WriteToLog("ucsAPI_EventFirmwareVersion " + ex.Message);
            }
        }

        void UCSCOMObj_EventTerminalDisconnected(int TerminalID)
        {
            try
            {
                var toDelete = TerminalList.Single(x => x.TerminalCode == TerminalID);
                if (toDelete != null)
                    TerminalList.Remove(toDelete);


                lbxMessage.Items.Insert(0, System.DateTime.Now + ".-." + "Terminal Disconnected TerminalID : " + TerminalID + " Code : " + this.ucsAPI.ErrorCode);
                objLogGenerator.WriteToLog("TerminalDisconnected TerminalID : " + TerminalID + " Code : " + this.ucsAPI.ErrorCode);
            }
            catch (Exception ex)
            {
                //objLogGenerator.WriteToLog("UCSCOMObj_EventTerminalDisconnected " + ex.Message);
            }
        }

        void ucsAPI_EventTerminalStatus(int ClientID, int TerminalID, int TerminalStatus, int DoorStatus, int CoverStatus)
        {
            try
            {
                if (lbxMessage.Items.Count > 100)
                    lbxMessage.Items.RemoveAt(lbxMessage.Items.Count - 1);

                //lbxMessage.Items.Insert(0, "Terminal - ClientID : " + ClientID + " TerminalID : " + TerminalID + " TerminalStatus : " + TerminalStatus + " Code : " + this.ucsAPI.ErrorCode);
                //objLogGenerator.WriteToLog("EventTerminal  - ClientID : " + ClientID + " TerminalID : " + TerminalID + " TerminalStatus : " + TerminalStatus + " Code : " + this.ucsAPI.ErrorCode);

                var toUpdate = TerminalList.Single(x => x.TerminalCode == TerminalID);
                if (toUpdate != null)
                    toUpdate.ConnectionTime = DateTime.Now;

                lbxMessage.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                objLogGenerator.WriteToLog("ucsAPI_EventTerminalStatus " + ex.Message);
            }
        }

        void ucsAPI_EventGetAccessLogCount(int ClientID, int TerminalID, int LogCount)
        {
            try
            {
                lbxMessage.Items.Insert(0, System.DateTime.Now + ".-." + "Get AccessLog Count - ClientID : " + ClientID + " TerminalID : " + TerminalID + " LogCount : " + LogCount + " Code : " + this.ucsAPI.ErrorCode);

                objLogGenerator.WriteToLog("EventGetAccessLogCount  - ClientID : " + ClientID + " TerminalID : " + ClientID + TerminalID + " LogCount : " + LogCount + " Code : " + this.ucsAPI.ErrorCode);

                if (LogCount > 0)
                {
                    accessLogData.GetAccessLogFromTerminal(0, TerminalID, 0);//0 = New Swipes,1 = OLD, 2 = ALL
                }

            }
            catch (Exception ex)
            {
                objLogGenerator.WriteToLog("ucsAPI_EventTerminalStatus " + ex.Message);
            }
        }

        void ucsAPI_EventGetAccessLog(int ClientID, int TerminalID)
        {
            try
            {
                lbxMessage.Items.Insert(0, System.DateTime.Now + ".-. " + "Get Access Log : " +
                        " TerminalID : " + TerminalID.ToString() +
                        " UserID : " + this.accessLogData.UserID +
                        " DataTime : " + this.accessLogData.DateTime +
                        " AuthMode : " + this.accessLogData.AuthMode +
                        " AuthType : " + this.accessLogData.AuthType +
                        " IsAuthorized : " + this.accessLogData.IsAuthorized +
                        " Result: " + this.accessLogData.AuthResult +
                        " RFID : " + this.accessLogData.RFID +
                        " ErrorCode : " + this.ucsAPI.ErrorCode +
                        " Progress : " + this.accessLogData.CurrentIndex + "/" + this.accessLogData.TotalNumber);

                objLogGenerator.WriteToLog("EventGetAccessLog " +
                    " TerminalID : " + TerminalID.ToString() +
                    " UserID : " + this.accessLogData.UserID +
                    " DataTime : " + this.accessLogData.DateTime +
                    " AuthMode : " + this.accessLogData.AuthMode +
                    " AuthType : " + this.accessLogData.AuthType +
                    " IsAuthorized : " + this.accessLogData.IsAuthorized +
                    " Result: " + this.accessLogData.AuthResult +
                    " RFID : " + this.accessLogData.RFID +
                    " ErrorCode : " + this.ucsAPI.ErrorCode +
                    " Progress : " + this.accessLogData.CurrentIndex + "/" + this.accessLogData.TotalNumber);
            }
            catch (Exception ex)
            {
                objLogGenerator.WriteToLog("ucsAPI_EventTerminalStatus " + ex.Message);
            }

            //lbxMessage.Items.Add("<--EventGetAccessLog");
            //lbxMessage.Items.Add(string.Format("   +ClientID:{0}", ClientID));
            //lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            //lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
            //lbxMessage.Items.Add(string.Format("   +UserID:{0}", this.accessLogData.UserID));
            //lbxMessage.Items.Add(string.Format("   +DateTime:{0}", this.accessLogData.DateTime));
            //lbxMessage.Items.Add(string.Format("   +AuthMode:{0}", this.accessLogData.AuthMode));
            //lbxMessage.Items.Add(string.Format("   +AuthType:{0}", this.accessLogData.AuthType));
            //lbxMessage.Items.Add(string.Format("   +IsAuthorized:{0}", this.accessLogData.IsAuthorized));
            //lbxMessage.Items.Add(string.Format("   +Result:{0}", this.accessLogData.AuthResult));
            //lbxMessage.Items.Add(string.Format("   +RFID:{0}", this.accessLogData.RFID));
            //lbxMessage.Items.Add(string.Format("   +PictureDataLength:{0}", this.accessLogData.PictureDataLength));
            //lbxMessage.Items.Add(string.Format("   +Progress:{0}/{1}", this.accessLogData.CurrentIndex, this.accessLogData.TotalNumber));

            //            ucsAPI.SetError(TerminalID, 1); // Test for Error

            //            byte[] biPictureData = null;
            //            long nPictureDataLength;

        }


        void ucsAPI_EventRealTimeAccessLog(int TerminalID)
        {
            try
            {
                bizterminalmessages terMessages = new bizterminalmessages();
                //TerminalMessages terminalMessages = new TerminalMessages();
                terMessages.TerminalID = TerminalID;
                terMessages.UserID = this.accessLogData.UserID;
                terMessages.MessageDateTime = Convert.ToDateTime(this.accessLogData.DateTime);
                terMessages.AuthMode = this.accessLogData.AuthMode;
                terMessages.AuthType = this.accessLogData.AuthType;
                terMessages.IsAuthorized = this.accessLogData.IsAuthorized;
                terMessages.DeviceID = this.accessLogData.DeviceID;
                terMessages.AuthResult = this.accessLogData.AuthResult;

                if (this.accessLogData.RFID == "")
                    terMessages.RFID = DBNull.Value;
                else
                    terMessages.RFID = this.accessLogData.RFID;

                terMessages.CurrentTime = DateTime.Now;
                terMessages.ErrorCode = this.ucsAPI.ErrorCode;
                //FOR PHOTO
                //int abc = this.accessLogData.PictureDataLength;
                //var strImage = this.accessLogData.PictureData;
                //byte[] btVisitorImages = new byte[10000];
                //btVisitorImages = (byte[])this.accessLogData.PictureData;
                //ByteArrayToImageFilebyMemoryStream(btVisitorImages);
                //terminalMessages.PictureData = btVisitorImages;

                ///CODE FOR CSV FILE GENERATION
                ///


                if (this.accessLogData.UserID > 0)
                {
                    bizvirdiuser objVirdi = new bizvirdiuser();
                    objVirdi.UserID = Convert.ToInt32(this.accessLogData.UserID);
                    objVirdi.SearchvirdiuserByID();

                    if (objVirdi.Dsvirdiuser.Tables[0].Rows.Count > 0)
                    {
                        bizfilepath objFile = new bizfilepath();
                        objFile.TerminalID = TerminalID;
                        objFile.SearchfilepathByTerminalID();

                        for (int i = 0; i < objFile.Dsfilepath.Tables[0].Rows.Count; i++)
                        {
                            CreateCSVFile(objFile.Dsfilepath.Tables[0].Rows[i]["filePath"].ToString(), Convert.ToInt32(terMessages.TerminalID), Convert.ToString(objVirdi.Dsvirdiuser.Tables[0].Rows[0]["userName"]), Convert.ToDateTime(terMessages.MessageDateTime), Convert.ToInt32(objVirdi.Dsvirdiuser.Tables[0].Rows[0]["userID"]), Convert.ToString(objVirdi.Dsvirdiuser.Tables[0].Rows[0]["oTLNumber"]));
                        }
                    }
                }


                terMessages.Insertterminalmessages();
                if (terMessages.Success == 1)
                {
                    lbxMessage.SelectedIndex = lbxMessage.Items.Count - 1;
                    if (lbxMessage.Items.Count > 50)
                        lbxMessage.Items.RemoveAt(lbxMessage.Items.Count - 1);

                    lbxMessage.Items.Insert(0, System.DateTime.Now + ".-. " + "Real Time Access Log : " +
                                       " TerminalID : " + TerminalID +
                                       " UserID : " + this.accessLogData.UserID +
                                       " DataTime : " + this.accessLogData.DateTime +
                                       " AuthMode : " + this.accessLogData.AuthMode +
                                       " AuthType : " + this.accessLogData.AuthType +
                                       " IsAuthorized : " + this.accessLogData.IsAuthorized +
                                       " Result : " + this.accessLogData.AuthResult +
                                       " RFID : " + this.accessLogData.RFID +
                                       " Progress: " + this.accessLogData.CurrentIndex + "/" + this.accessLogData.TotalNumber +
                                       " ErrorCode : " + this.ucsAPI.ErrorCode);

                    objLogGenerator.WriteToLog("EventRealTimeAccessLog " +
                        " TerminalID : " + TerminalID +
                        " UserID : " + this.accessLogData.UserID +
                        " DataTime : " + this.accessLogData.DateTime +
                        " AuthMode : " + this.accessLogData.AuthMode +
                        " AuthType : " + this.accessLogData.AuthType +
                        " IsAuthorized : " + this.accessLogData.IsAuthorized +
                        " Result : " + this.accessLogData.AuthResult +
                        " RFID : " + this.accessLogData.RFID +
                        " Progress: " + this.accessLogData.CurrentIndex + "/" + this.accessLogData.TotalNumber +
                        " ErrorCode : " + this.ucsAPI.ErrorCode);
                }

                //string jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(terminalMessages);
                //HttpContent content = new StringContent(jsonstring);
                //content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                //using (var client = new HttpClient())
                //{
                //    client.BaseAddress = new Uri(System.Configuration.ConfigurationSettings.AppSettings["ApiPath"].ToString());
                //    HttpResponseMessage response = new HttpResponseMessage();
                //    response = client.PostAsync("TerminalMessages/UpsertTerminalMessages", content).Result;
                //    List<Messages> messages = response.Content.ReadAsAsync<List<Messages>>().Result;

                //    if (messages != null)
                //    {
                //        switch (messages.ElementAt(0).Result)
                //        {
                //            case 0://-->Exception in SQL
                //                lbxMessage.Items.Insert(0, System.DateTime.Now + ".-. " + messages[0].MessageDescription);
                //                break;
                //            case 1://-->Already exists
                //                lbxMessage.Items.Insert(0, System.DateTime.Now + ".-. " + messages[0].MessageDescription);
                //                break;
                //            case 2://-->Success
                //                lbxMessage.Items.Insert(0, System.DateTime.Now + ".-. " + messages[0].MessageDescription);
                //                break;
                //            default:
                //                break;
                //        }
                //    }
                //}



                //using (var client = new HttpClient())
                //{
                //    client.BaseAddress = new Uri(System.Configuration.ConfigurationSettings.AppSettings["ApiPath"].ToString());
                //    HttpResponseMessage response = new HttpResponseMessage();
                //    response = client.GetAsync("EmployeeEnrollmentDetails/UpdateEmployeeEnrollmentDetailsforPushedInDevice?EmployeeID=11&IsPushedInDevice=1").Result;
                //    List<Messages> messages = response.Content.ReadAsAsync<List<Messages>>().Result;

                //    if (messages != null)
                //    {
                //        switch (messages.ElementAt(0).Result)
                //        {
                //            case 0://-->Exception in SQL
                //                lbxMessage.Items.Insert(0, System.DateTime.Now + ".-. " + messages[0].MessageDescription);
                //                break;
                //            case 1://-->Already exists
                //                lbxMessage.Items.Insert(0, System.DateTime.Now + ".-. " + messages[0].MessageDescription);
                //                break;
                //            case 2://-->Success
                //                lbxMessage.Items.Insert(0, System.DateTime.Now + ".-. " + messages[0].MessageDescription);
                //                break;
                //            default:
                //                break;
                //        }
                //    }
                //}


            }
            catch (Exception ex)
            {
                objLogGenerator.WriteToLog("Error at ucsAPI_EventRealTimeAccessLog " + ex.Message);
            }

        }

        void ucBioBSP_OnEnrollEvent(int EventID)
        {
            lbxMessage.Items.Add(string.Format("<--Bio Enroll : EventID({0})", EventID));
            lbxMessage.Items.Add(string.Format("   +EventID:{0}", EventID));
        }

        void ucBioBSP_OnCaptureEvent(int Quality)
        {
            lbxMessage.Items.Add(string.Format("<--Bio Capture : Quality({0})", Quality));
            lbxMessage.Items.Add(string.Format("   +Quality:{0}", Quality));
        }

        void ucsAPI_EventVerifyPassword(int TerminalID, int UserID, int AuthMode, int AntipassbackLevel, string Password)
        {
            int IsAuthorized;
            string txtEventTime;
            //if (chkPassword.Checked)
            //    IsAuthorized = 1;
            //else
            IsAuthorized = 0;
            txtEventTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            this.serverAuthentication.SendAuthResultToTerminal(Convert.ToInt32(this.terminalID), 1, 1, 0, IsAuthorized, txtEventTime, 0);

            lbxMessage.Items.Add("<--EventVerifyPassword");
            lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +UserID:{0}", UserID));
            lbxMessage.Items.Add(string.Format("   +AuthMode:{0}", AuthMode));
            lbxMessage.Items.Add(string.Format("   +Antipassback Level:{0}", AntipassbackLevel));
            lbxMessage.Items.Add(string.Format("   +Password:{0}", Password));
        }


        /*
        typedef UCBioAPI_UINT16   UCBioAPI_FIR_PURPOSE;
        #define UCBioAPI_FIR_PURPOSE_VERIFY                          (0x01)
        #define UCBioAPI_FIR_PURPOSE_IDENTIFY                        (0x02)
        #define UCBioAPI_FIR_PURPOSE_ENROLL                          (0x03)
        #define UCBioAPI_FIR_PURPOSE_ENROLL_FOR_VERIFICATION_ONLY    (0x04)
        #define UCBioAPI_FIR_PURPOSE_ENROLL_FOR_IDENTIFICATION_ONLY  (0x05)
        #define UCBioAPI_FIR_PURPOSE_AUDIT                           (0x06)
        #define UCBioAPI_FIR_PURPOSE_UPDATE                          (0x10)
        */
        //void ucsAPI_EventVerifyFinger1toN(int TerminalID, int AuthMode, int InputIDLength, int SecurityLevel, int AntipassbackLevel, object FingerData)
        //{
        //    // Always compare the fingerprints contained in the file(*.UFS) is read and the current fingerprint.
        //    lbxMessage.Items.Add("<--EventVerifyFinger1toN");

        //    int IsAuthorized;
        //    string txtEventTime;
        //    int IsFinger;
        //    int IsFPCard;
        //    int IsPassword;
        //    int IsCard;
        //    int IsCardID;
        //    int IsAndOperation;

        //    IsAndOperation = Convert.ToInt32(chkAndOperation.Checked);
        //    IsCardID = Convert.ToInt32(chkCardID.Checked);
        //    IsCard = Convert.ToInt32(chkCard.Checked);
        //    IsPassword = Convert.ToInt32(chkPassword.Checked);
        //    IsFPCard = Convert.ToInt32(chkFPCard.Checked);
        //    IsFinger = Convert.ToInt32(chkFingerprint.Checked);

        //    int authErrorCode = 770;// 매칭 실패

        //    if (IsFinger == 1)
        //        IsAuthorized = 1;
        //    else
        //        IsAuthorized = 0;

        //    if (IsAuthorized == 1)
        //    {
        //        fpData.Import(1, 1, 2, 400, 400, FingerData, null);

        //        string szCapturedFIR = fpData.TextFIR;
        //        fastSearch.IdentifyUser(szCapturedFIR, 5);
        //        if (ucBioBSP.ErrorCode == 0)
        //        {
        //            // 인증 성공
        //            IsAuthorized = 1;
        //            authErrorCode = 0;
        //        }
        //        else
        //        {
        //            //인증 실패
        //            IsAuthorized = 0;
        //            authErrorCode = 770;
        //        }
        //    }
        //    lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
        //    lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
        //    lbxMessage.Items.Add(string.Format("   +AuthMode:{0}", AuthMode));
        //    lbxMessage.Items.Add(string.Format("   +InputID Length:{0}", InputIDLength));
        //    lbxMessage.Items.Add(string.Format("   +Security Level:{0}", SecurityLevel));
        //    lbxMessage.Items.Add(string.Format("   +Antipassback Level:{0}", AntipassbackLevel));
        //    lbxMessage.Items.Add(string.Format("   +FingerData:{0}", FingerData));

        //    txtEventTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
        //    this.serverAuthentication.SetAuthType(IsAndOperation, IsFinger, IsFPCard, IsPassword, IsCard, IsCardID);
        //    this.serverAuthentication.SendAuthResultToTerminal(Convert.ToInt32(this.terminalID), 1, 1, 0, IsAuthorized, txtEventTime, authErrorCode);
        //}

        //void ucsAPI_EventVerifyFinger1to1(int TerminalID, int UserID, int AuthMode, int SecurityLevel, int AntipassbackLevel, object FingerData)
        //{
        //    // Always compared with the finger immediately before the terminal input.
        //    lbxMessage.Items.Add("<--EventVerifyFinger1to1");

        //    this.ucBioBSP.SecurityLevelForVerify = 5;

        //    int IsAuthorized;
        //    string txtEventTime;
        //    if (chkFingerprint.Checked)
        //        IsAuthorized = 1;
        //    else
        //        IsAuthorized = 0;

        //    int authErrorCode = 770;// 매칭 실패

        //    if (IsAuthorized == 1)
        //    {
        //        fpData.Import(1, 1, 1, 400, 400, FingerData, null);

        //        string szCapturedFIR = fpData.TextFIR;

        //        matching.VerifyMatch(szTextEnrolledFIR, szCapturedFIR);

        //        if (matching.MatchingResult == 0)
        //        {
        //            //인증 실패
        //            IsAuthorized = 0;
        //            authErrorCode = 770;
        //            lbxMessage.Items.Add(string.Format("   +Matching Fail."));
        //        }
        //        else
        //        {
        //            // 인증 성공
        //            authErrorCode = 0;
        //            IsAuthorized = 1;
        //            lbxMessage.Items.Add(string.Format("   +Matching Success."));
        //        }
        //        // 이전 지문으로 보존
        //        szTextEnrolledFIR = szCapturedFIR;
        //    }
        //    lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
        //    lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
        //    lbxMessage.Items.Add(string.Format("   +UserID:{0}", UserID));
        //    lbxMessage.Items.Add(string.Format("   +AuthMode:{0}", AuthMode));
        //    lbxMessage.Items.Add(string.Format("   +Security Level:{0}", SecurityLevel));
        //    lbxMessage.Items.Add(string.Format("   +Antipassback Level:{0}", AntipassbackLevel));
        //    lbxMessage.Items.Add(string.Format("   +FingerData:{0}", FingerData));

        //    txtEventTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
        //    this.serverAuthentication.SendAuthResultToTerminal(Convert.ToInt32(this.terminalID), 1, 1, 0, IsAuthorized, txtEventTime, authErrorCode);
        //}

        //void ucsAPI_EventVerifyCard(int TerminalID, int AuthMode, int AntipassbackLevel, string TextRFID)
        //{
        //    int IsAuthorized;
        //    string txtEventTime;
        //    int IsFinger;
        //    int IsFPCard;
        //    int IsPassword;
        //    int IsCard;
        //    int IsCardID;
        //    int IsAndOperation;

        //    IsAndOperation = Convert.ToInt32(chkAndOperation.Checked);
        //    IsCardID = Convert.ToInt32(chkCardID.Checked);
        //    IsCard = Convert.ToInt32(chkCard.Checked);
        //    IsPassword = Convert.ToInt32(chkPassword.Checked);
        //    IsFPCard = Convert.ToInt32(chkFPCard.Checked);
        //    IsFinger = Convert.ToInt32(chkFingerprint.Checked);

        //    if (IsCard == 1 || IsCardID == 1)
        //        IsAuthorized = 1;
        //    else
        //        IsAuthorized = 0;
        //    txtEventTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
        //    this.serverAuthentication.SetAuthType(IsAndOperation, IsFinger, IsFPCard, IsPassword, IsCard, IsCardID);
        //    this.serverAuthentication.SendAuthResultToTerminal(Convert.ToInt32(this.terminalID), 1, 1, 0, IsAuthorized, txtEventTime, 0);

        //    lbxMessage.Items.Add("<--EventVerifyCard");
        //    lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
        //    lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
        //    lbxMessage.Items.Add(string.Format("   +AuthMode:{0}", AuthMode));
        //    lbxMessage.Items.Add(string.Format("   +Antipassback Level:{0}", AntipassbackLevel));
        //    lbxMessage.Items.Add(string.Format("   +TextRFID:{0}", TextRFID));
        //}

        void ucsAPI_EventSetTATime(int ClientID, int TerminalID)
        {
            lbxMessage.Items.Add("<--EventSetTATime");
            lbxMessage.Items.Add(string.Format("   +ClientID:{0}", ClientID));
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
        }

        void ucsAPI_EventSetTAFunction(int ClientID, int TerminalID)
        {
            lbxMessage.Items.Add("<--EventSetTAFunction");
            lbxMessage.Items.Add(string.Format("   +ClientID:{0}", ClientID));
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
        }

        void ucsAPI_EventSetAccessControlData(int ClientID, int TerminalID, int DataType)
        {
            lbxMessage.Items.Add("<--EventSetAccessControlData");
            lbxMessage.Items.Add(string.Format("   +ClientID:{0}", ClientID));
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +DataType:{0}", DataType));
            lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
        }

        void ucsAPI_EventPictureLog(int TerminalID)
        {
            lbxMessage.Items.Add("<--EventPictureLog");
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", this.terminalID));
            lbxMessage.Items.Add(string.Format("   +UserID:{0}", this.accessLogData.UserID));
            lbxMessage.Items.Add(string.Format("   +DataTime:{0}", this.accessLogData.DateTime));
            lbxMessage.Items.Add(string.Format("   +AuthMode:{0}", this.accessLogData.AuthMode));
            lbxMessage.Items.Add(string.Format("   +AuthType:{0}", this.accessLogData.AuthType));
            lbxMessage.Items.Add(string.Format("   +IsAuthorized:{0}", this.accessLogData.IsAuthorized));
            lbxMessage.Items.Add(string.Format("   +Result:{0}", this.accessLogData.AuthResult));
            lbxMessage.Items.Add(string.Format("   +RFID:{0}", this.accessLogData.RFID));
            lbxMessage.Items.Add(string.Format("   +PictureDataLength:{0}", this.accessLogData.PictureDataLength));
            lbxMessage.Items.Add(string.Format("   +Progress:{0}/{1}", this.accessLogData.CurrentIndex, this.accessLogData.TotalNumber));
        }

        void ucsAPI_EventOpenDoor(int ClientID, int TerminalID)
        {
            lbxMessage.Items.Add("<--EventOpenDoor");
            lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
            lbxMessage.Items.Add(string.Format("   +ClientID:{0}", ClientID));
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
        }

        void ucsAPI_EventGetUserInfoList(int ClientID, int TerminalID)
        {
            lbxMessage.Items.Add("<--EventGetUserInfoList");
            lbxMessage.Items.Add(string.Format("   +ClientID:{0}", ClientID));
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
            lbxMessage.Items.Add(string.Format("   +UserID:{0}", this.terminalUserData.UserID));
            lbxMessage.Items.Add(string.Format("   +Admin:{0}", this.terminalUserData.IsAdmin));
            lbxMessage.Items.Add(string.Format("   +AuthType:{0}", this.terminalUserData.AuthType));
            lbxMessage.Items.Add(string.Format("   +Progress:{0}/{1}", this.terminalUserData.CurrentIndex, this.terminalUserData.TotalNumber));
        }

        void ucsAPI_EventGetUserData(int ClientID, int TerminalID)
        {
            byte[] biFPData1 = null;
            byte[] biFPData2 = null;
            byte[] biFPData11 = null;
            byte[] biFPData22 = null;
            long nFPDataSize1;
            long nFPDataSize2;
            long nFPDataCount;

            nFPDataCount = this.terminalUserData.TotalFingerCount;
            for (int i = 0; i < nFPDataCount; i++)
            {
                if (i == 0)
                {
                    int nFingerID = this.terminalUserData.get_FingerID(i);
                    nFPDataSize1 = this.terminalUserData.get_FPSampleDataLength(nFingerID, 0);
                    biFPData1 = this.terminalUserData.get_FPSampleData(nFingerID, 0) as byte[];
                    nFPDataSize2 = this.terminalUserData.get_FPSampleDataLength(nFingerID, 1);
                    biFPData2 = this.terminalUserData.get_FPSampleData(nFingerID, 1) as byte[];
                }
                else if (i == 1)
                {
                    int nFingerID = this.terminalUserData.get_FingerID(i);
                    nFPDataSize1 = this.terminalUserData.get_FPSampleDataLength(nFingerID, 0);
                    biFPData11 = this.terminalUserData.get_FPSampleData(nFingerID, 0) as byte[];
                    nFPDataSize2 = this.terminalUserData.get_FPSampleDataLength(nFingerID, 1);
                    biFPData22 = this.terminalUserData.get_FPSampleData(nFingerID, 1) as byte[];
                }
            }

            //lbxMessage.Items.Add("<--EventGetUserData");
            //lbxMessage.Items.Add(string.Format("   +ClientID:{0}", ClientID));
            //lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            //lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
            //lbxMessage.Items.Add(string.Format("   +UserID:{0}", this.terminalUserData.UserID));
            //lbxMessage.Items.Add(string.Format("   +Admin:{0}", this.terminalUserData.IsAdmin));
            //lbxMessage.Items.Add(string.Format("   +AccessGroup:{0}", this.terminalUserData.AccessGroup));
            //lbxMessage.Items.Add(string.Format("   +AccessDateType:{0}", this.terminalUserData.AccessDateType));
            //lbxMessage.Items.Add(string.Format("   +AccessDate:{0}~{1}", this.terminalUserData.StartAccessDate, this.terminalUserData.EndAccessDate));
            //lbxMessage.Items.Add(string.Format("   +AuthType:{0}", this.terminalUserData.AuthType));
            //lbxMessage.Items.Add(string.Format("   +Password:{0}", this.terminalUserData.Password));
            //lbxMessage.Items.Add(string.Format("   +TotalFingerCount:{0}", nFPDataCount));

            string strRFID = "";
            for (int i = 0; i < this.terminalUserData.CardNumber; i++)
            {
                strRFID = this.terminalUserData.get_RFID(i);
            }

            bizvirdiuser objViruser = new bizvirdiuser();
            objViruser.UserID = this.terminalUserData.UserID;
            objViruser.FingerOne = biFPData1;
            objViruser.FingerOneCopy = biFPData2;
            objViruser.FingerTwo = biFPData11;
            objViruser.FingerTwoCopy = biFPData22;
            objViruser.RfidCard = strRFID;
            objViruser.UpdatevirdiuserEnrolment();

            if (objViruser.Success == 1)
            {
                //bizenrolment objEnr = new bizenrolment();
                //objEnr.TerminalID = TerminalID;
                //objEnr.UserID = this.terminalUserData.UserID;
                //objEnr.TransMsg = "User Data Captured. Terminal " + TerminalID + " is not connected.";
                //objEnr.EnrolFlag = 2;//2 =Data Captured
                //objEnr.UpdateenrolmentforFlag();

                //if (objEnr.Success == 1)
                {
                    lbxMessage.Items.Insert(0, System.DateTime.Now + ".-." + "User Data Captured.");
                    //EnrolUser(this.terminalUserData.UserID);
                }
            }
        }

        void ucsAPI_EventGetUserCount(int ClientID, int TerminalID, int AdminCount, int UserCount)
        {
            lbxMessage.Items.Add("<--EventGetUserCount");
            lbxMessage.Items.Add(string.Format("   +CID, TID : {0}, {1}", ClientID, TerminalID));
            lbxMessage.Items.Add(string.Format("   +Admin, User : {0}, {1}", AdminCount, UserCount));
        }

        void ucsAPI_EventGetTAFunction(int ClientID, int TerminalID, int TAMode)
        {
            lbxMessage.Items.Add("<--EventGetTAFunction");
            lbxMessage.Items.Add(string.Format("   +ClientID:{0}", ClientID));
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +TAMode:{0}", TAMode));
        }


        void ucsAPI_EventFirmwareUpgrading(int ClientID, int TerminalID, int CurrentIndex, int TotalNumber)
        {
            lbxMessage.Items.Add("<--EventFirmwareUpgrading");
            lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
            lbxMessage.Items.Add(string.Format("   +ClientID:{0}", ClientID));
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +CurrentIndex:{0}", CurrentIndex));
            lbxMessage.Items.Add(string.Format("   +TotalNumber:{0}", TotalNumber));
        }

        void ucsAPI_EventFirmwareUpgraded(int ClientID, int TerminalID)
        {
            lbxMessage.Items.Add("<--EventFirmwareUpgraded");
            lbxMessage.Items.Add(string.Format("   +ClientID:{0}", ClientID));
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
        }

        void ucsAPI_EventFingerImageData(int TerminalID)
        {
            lbxMessage.Items.Add("<--EventFingerImageData");
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
        }

        void ucsAPI_EventDeleteUser(int ClientID, int TerminalID, int UserID)
        {
            //lbxMessage.Items.Add("<--EventDeleteUser");
            //lbxMessage.Items.Add(string.Format("   +ClientID:{0}", ClientID));
            //lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            //lbxMessage.Items.Add(string.Format("   +UserID:{0}", UserID));
            //lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));

            bizenrolment objEnr = new bizenrolment();
            objEnr.TerminalID = TerminalID;
            objEnr.UserID = UserID;
            objEnr.TransMsg = "User Deleted Successfully.";
            objEnr.EnrolFlag = 6;//6=Deleted Successfuly
            objEnr.UpdateenrolmentforFlag();
            if (objEnr.Success == 1)
            {
                lbxMessage.Items.Insert(0, System.DateTime.Now + ".-." + "User Successfully Deleted from TerminalID : " + TerminalID + " ClientID : " + ClientID + " UserID : " + UserID + " Code : " + this.ucsAPI.ErrorCode);
                objLogGenerator.WriteToLog("User Successfully Deleted from TerminalID : " + TerminalID + " ClientID : " + ClientID + " UserID : " + UserID + " Code : " + this.ucsAPI.ErrorCode);
            }
        }

        void ucsAPI_EventDeleteAllUser(int ClientID, int TerminalID)
        {
            lbxMessage.Items.Add("<--EventDeleteAllUser");
            lbxMessage.Items.Add(string.Format("   +ClientID:{0}", ClientID));
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
        }

        void ucsAPI_EventControlPeripheralDevice(int Handle, int TerminalID, int PeripheralDeviceID, int ControlCommand)
        {
            lbxMessage.Items.Add("<--EventControlPeripheralDevice");
            lbxMessage.Items.Add(string.Format("   +Handle:{0}", Handle));
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +PeripheralDeviceID:{0}", PeripheralDeviceID));
            lbxMessage.Items.Add(string.Format("   +ControlCommand:{0}", ControlCommand));
        }

        //void ucsAPI_EventAuthTypeWithUserID(int TerminalID, int UserID)
        //{
        //    CallSetAuthTypeAndSendAuthInfoToTerminal();

        //    lbxMessage.Items.Add("<--EventAuthTypeWithUserID");
        //    lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
        //    lbxMessage.Items.Add(string.Format("   +UserID:{0}", UserID));
        //}


        //void ucsAPI_EventAuthTypeWithUniqueID(int TerminalID, string UniqueID)
        //{
        //    CallSetAuthTypeAndSendAuthInfoToTerminal();

        //    lbxMessage.Items.Add("<--EventAuthTypeWithUniqueID");
        //    lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
        //    lbxMessage.Items.Add(string.Format("   +UniqueID:{0}", UniqueID));
        //    lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
        //}

        void ucsAPI_EventAntipassback(int TerminalID, int UserID)
        {
            lbxMessage.Items.Add("<--EventAntipassback");
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +UserID:{0}", UserID));
            int Result = 1; // 0 = Faile, 1 = Success
            this.serverAuthentication.SendAntipassbackResultToTerminal(TerminalID, UserID, Result);
            lbxMessage.Items.Add("-->SendAntipassbackResultToTerminal");
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +UserID:{0}", UserID));
            lbxMessage.Items.Add(string.Format("   +Result:{0}", Result));
        }


        void ucsAPI_EventAddUser(int ClientID, int TerminalID, int UserID)
        {
            bizenrolment objEnr = new bizenrolment();
            objEnr.TerminalID = TerminalID;
            objEnr.UserID = UserID;
            objEnr.TransMsg = "User Added Successfully.";
            objEnr.EnrolFlag = 4;//4=Enrolled Successfuly
            objEnr.UpdateenrolmentforFlag();
            if (objEnr.Success == 1)
            {
                lbxMessage.Items.Insert(0, System.DateTime.Now + ".-." + "User Successfully Added TerminalID : " + TerminalID + " ClientID : " + ClientID + " UserID : " + UserID + " Code : " + this.ucsAPI.ErrorCode);
                objLogGenerator.WriteToLog("User Successfully Added TerminalID : " + TerminalID + " ClientID : " + ClientID + " UserID : " + UserID + " Code : " + this.ucsAPI.ErrorCode);
            }
        }

        void ucsAPI_EventUserFileUpgrading(int ClientID, int TerminalID, int CurrentIndex, int TotalNumber)
        {
            lbxMessage.Items.Add("<--EventUserFileUpgrading");
            lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
            lbxMessage.Items.Add(string.Format("   +ClientID:{0}", ClientID));
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +CurrentIndex:{0}", CurrentIndex));
            lbxMessage.Items.Add(string.Format("   +TotalNumber:{0}", TotalNumber));
        }

        void ucsAPI_EventUserFileUpgraded(int ClientID, int TerminalID)
        {
            lbxMessage.Items.Add("<--EventUserFileUpgraded");
            lbxMessage.Items.Add(string.Format("   +ClientID:{0}", ClientID));
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
        }

        void ucsAPI_EventPrivateMessage(int TerminalID, int Reserved)
        {
            lbxMessage.Items.Add("<--EventPrivateMessage");
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
        }

        void ucsAPI_EventPublicMessage(int ClientID, int TerminalID)
        {
            lbxMessage.Items.Add("<--EventPublicMessage");
            lbxMessage.Items.Add(string.Format("   +ClientID:{0}", ClientID));
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +ErrorCode:{0}", this.ucsAPI.ErrorCode));
        }

        void ucsAPI_EventEmergency(int TerminalID, int SignalType, int SignalValue, int Reserved1, int Reserved2)
        {
            lbxMessage.Items.Add("<--EventEmergency");
            lbxMessage.Items.Add(string.Format("   +TerminalID:{0}", TerminalID));
            lbxMessage.Items.Add(string.Format("   +SignalType:{0}", SignalType));
            lbxMessage.Items.Add(string.Format("   +SignalValue:{0}", SignalValue));
        }

        void ucsAPI_EventSetEmergency(int ClientID, int TerminalID)
        {
            lbxMessage.Items.Add("<--EventSetEmergency");
            lbxMessage.Items.Add(string.Format("   +CID, TID : {0}, {1}", ClientID, TerminalID));
        }

        void ucsAPI_EventTerminalControl(int ClientID, int TerminalID, int lockStatus, int lockType)
        {
            lbxMessage.Items.Add("<--EventTerminalControl");
            lbxMessage.Items.Add(string.Format("   +CID, TID : {0}, {1}", ClientID, TerminalID));
            lbxMessage.Items.Add(string.Format("   +lockStatus , lockType : {0}, {1}", lockStatus, lockType));
        }

        void ucsAPI_EventRegistFace(int ClientID, int TerminalID, int CurrentIndex, int TotalNumber, object EventData)
        {
            byte[] RegFaceData = (byte[])EventData;

            if (CurrentIndex == 0 && TotalNumber == 0)
            {
                lbxMessage.Items.Add("<--EventRegistFace");
                lbxMessage.Items.Add(string.Format("   +CID, TID : {0}, {1}", ClientID, TerminalID));
                lbxMessage.Items.Add(string.Format("   Process Canceled.({0})", EventData.GetType()));
            }
            else
            {
                lbxMessage.Items.Add("<--EventRegistFace");
                lbxMessage.Items.Add(string.Format("   +CID, TID : {0}, {1}", ClientID, TerminalID));
                lbxMessage.Items.Add(string.Format("   +Process(Current, Total) : {0}, {1}", CurrentIndex, TotalNumber));
                lbxMessage.Items.Add(string.Format("   +RegFace Length : {0}", RegFaceData.Length));
            }
        }

        //private void btnSendMsg_Click(object sender, RoutedEventArgs e)
        //{
        //    txtStartDate = txtEndDate = DateTime.Now.ToString("yyyy-MM-dd");
        //    txtStartTime = "15:00";
        //    txtEndTime = txtToTime.Text;
        //    txtMessage = txtMsg.Text.Trim();
        //    int ShowFlag = 1; // 0 = Clear, 1 = Show



        //    foreach (Terminal Term in TerminalList)
        //    {
        //        ucsAPI.SendPublicMessageToTerminal(0, Term.TerminalCode, ShowFlag, txtStartDate, txtEndDate, txtStartTime, txtEndTime, txtMessage);
        //        lbxMessage.Items.Add("-->SendPublicMessageToTerminal");
        //        lbxMessage.Items.Add("   +ErrorCode :" + this.ucsAPI.ErrorCode.ToString("X4"));
        //        lbxMessage.Items.Insert(0, " Message Sent to : Terminal : " + Term.TerminalCode);
        //    }


        //}

        //private void CallSetAuthTypeAndSendAuthInfoToTerminal()
        //{
        //    int IsFinger;
        //    int IsFPCard;
        //    int IsPassword;
        //    int IsCard;
        //    int IsCardID;
        //    int IsAndOperation;

        //    IsAndOperation = Convert.ToInt32(chkAndOperation.Checked);
        //    IsFPCard = Convert.ToInt32(chkFPCard.Checked);
        //    IsCard = Convert.ToInt32(chkCard.Checked);
        //    IsPassword = Convert.ToInt32(chkPassword.Checked);
        //    IsCardID = Convert.ToInt32(chkCard.Checked);
        //    IsFinger = Convert.ToInt32(chkFingerprint.Checked);

        //    this.serverAuthentication.SetAuthType(IsAndOperation, IsFinger, IsFPCard, IsPassword, IsCard, IsCardID);
        //    this.serverAuthentication.SendAuthInfoToTerminal(Convert.ToInt32(this.terminalID), 1, 1, 0);
        //}

        #endregion

        #region PushingUserInTerminal

        #region Reading Server Port

        private void ReadingServerPort()
        {
            TcpListener server = null;
            try
            {
                while (true)
                {
                    // Set the TcpListener on port 9999.
                    Int32 port = 9999;
                    IPAddress localAddr = IPAddress.Parse("192.168.2.40");

                    // TcpListener server = new TcpListener(port);
                    server = new TcpListener(localAddr, port);

                    // Start listening for client requests.
                    server.Start();

                    // Buffer for reading data
                    Byte[] bytes = new Byte[256];
                    String data = null;

                    // Enter the listening loop. 
                    while (true)
                    {
                        //Console.Write("Waiting for a connection... ");
                        lbxMessage.Invoke((Action)(() => lbxMessage.Items.Insert(0, System.DateTime.Now + ".-. " + "Waiting for a connection....")));
                        // Perform a blocking call to accept requests. 
                        // You could also user server.AcceptSocket() here.
                        TcpClient client = server.AcceptTcpClient();
                        lbxMessage.Invoke((Action)(() => lbxMessage.Items.Insert(0, System.DateTime.Now + ".-. " + "Connected....")));

                        data = null;

                        // Get a stream object for reading and writing
                        NetworkStream stream = client.GetStream();

                        int i;

                        // Loop to receive all the data sent by the client. 
                        while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                        {
                            // Translate data bytes to a ASCII string.
                            data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                            string[] recidevedData = data.Split('^');

                            PushEmployeeFingerPrint(recidevedData[0], recidevedData[1]);


                            lbxMessage.Invoke((Action)(() => lbxMessage.Items.Insert(0, System.DateTime.Now + ".-. " + "Received.... " + data)));
                            //Console.WriteLine("Received: {0}", data);

                            // Process the data sent by the client.
                            data = data.ToUpper();

                            byte[] msg = System.Text.Encoding.ASCII.GetBytes(data);

                            // Send back a response.
                            stream.Write(msg, 0, msg.Length);

                            //Console.WriteLine("Sent: {0}", data);
                        }

                        // Shutdown and end connection
                        client.Close();
                    }
                }
            }
            catch (SocketException e)
            {
                objLogGenerator.WriteToLog("Error at ReadingServerPort SocketException" + e.Message);
            }
            catch (Exception ex)
            {
                objLogGenerator.WriteToLog("Error at ReadingServerPort Exception" + ex.Message);

            }
            finally
            {
                // Stop listening for new clients.
                server.Stop();
                ReadingServerPort();
            }
        }

        #endregion Reading Server Port

        private void PushEmployeeFingerPrint(string EmpID, string TermID)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    List<EmployeeEnrollmentDetailsView> objEmployeDetail;
                    client.BaseAddress = new Uri(System.Configuration.ConfigurationSettings.AppSettings["ApiPath"].ToString());
                    HttpResponseMessage response = client.GetAsync("EmployeeEnrollmentDetails/GetEmployeeEnrollmentDetails?EmployeeID=" + EmpID).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        objEmployeDetail = response.Content.ReadAsAsync<List<EmployeeEnrollmentDetailsView>>().Result;

                        foreach (EmployeeEnrollmentDetailsView objEmployeePush in objEmployeDetail)
                        {
                            serveruserData.InitUserData();
                            serveruserData.UserID = Convert.ToInt32(EmpID);
                            serveruserData.UniqueID = EmpID;
                            serveruserData.UserName = objEmployeePush.EmpName;
                            serveruserData.IsAdmin = 0;
                            serveruserData.IsIdentify = 1;
                            serveruserData.IsFace1toN = 0;
                            serveruserData.AuthType = 1;
                            serveruserData.SetAuthType(0, 1, 0, 0, 0, 0);

                            byte[] biFPData1_1 = new byte[500];
                            byte[] biFPData1_2 = new byte[500];
                            byte[] biFPData2_1 = new byte[500];
                            byte[] biFPData2_2 = new byte[500];

                            biFPData1_1 = objEmployeePush.EmpEnrFinger1;
                            biFPData1_2 = objEmployeePush.EmpEnrFinger1Copy;
                            biFPData2_1 = objEmployeePush.EmpEnrFinger2;
                            biFPData2_2 = objEmployeePush.EmpEnrFinger2Copy;

                            int nFingerID1 = 1;
                            int nFingerID2 = 2;

                            serveruserData.AddFingerData((int)nFingerID1, (int)nTemplateType400, biFPData1_1, biFPData1_2);
                            serveruserData.AddFingerData((int)nFingerID2, (int)nTemplateType400, biFPData2_1, biFPData2_2);

                            serveruserData.SetDuressFinger(1, 1);
                            serveruserData.AddUserToTerminal(1, Convert.ToInt32(TermID), 1);


                            lbxMessage.Invoke((Action)(() => lbxMessage.Items.Insert(0, System.DateTime.Now + ".-. Successfully Pushed....")));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objLogGenerator.WriteToLog("Error at PushEmployeeFingerPrint" + ex.Message);
            }

        }

        #endregion

        #region CSV Generation
        //Creating or Replacing CSV file at Given Server path.
        private void CreateCSVFile(string DestPath, int TerminalID, string UserName, DateTime ReadingTime, int UserID, string oTLNumber)
        {
            try
            {
                var strcsv = new StringBuilder();
                var newLine = string.Format("{0},{1},{2},{3}", Convert.ToString(TerminalID), UserName.ToUpper(), ReadingTime.ToString("dd/MM/yyyy") + " " + ReadingTime.ToString("HH:mm:ss"), oTLNumber.ToUpper(), Environment.NewLine);
                strcsv.Append(newLine);
                File.WriteAllText(DestPath, strcsv.ToString());

                lbxMessage.Items.Insert(0, System.DateTime.Now + ".-. Record Copied for : " + UserName + " at : " + DestPath);
                objLogGenerator.WriteToLog(System.DateTime.Now + ".-. Record Copied for : " + UserName + " at : " + DestPath);
            }
            catch (Exception ex)
            {
                objLogGenerator.WriteToLog("Error in CreateCSVFile: " + ex.Message);
            }
        }

        #endregion CSV Generation

        #region ManualEnrolment
        private void UserEnrolment()
        {
            try
            {
                //while (true)
                {
                    bizvirdiuser objEnrollment = new bizvirdiuser();
                    objEnrollment.SearchEnrolment();

                    if (objEnrollment.Dsvirdiuser.Tables[0].Rows.Count > 0)
                    {
                        if (objEnrollment.Dsvirdiuser.Tables[0].Rows[0][0].ToString() == "E")
                        {
                            //EnrolUser(objEnrollment.Dsvirdiuser);
                            //Convert.ToString(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["userID"]),
                            //Convert.ToString(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["userName"]),
                            //(byte[])objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["fingerOne"],
                            //(byte[])objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["fingerOneCopy"],
                            //(byte[])objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["fingerTwo"],
                            //(byte[])objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["fingerTwoCopy"],
                            //Convert.ToInt32(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["terminalID"]));

                            lbxMessage.Invoke((Action)(() => lbxMessage.Items.Insert(0, System.DateTime.Now + ".-. " + Convert.ToString(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["userName"]) + " Successfully Pushed....")));
                        }
                        else if (objEnrollment.Dsvirdiuser.Tables[0].Rows[0][0].ToString() == "D")
                        {
                        }
                        //tskEnrolment.Wait(9000000);
                    }


                    //if (toDelete != null)
                    //{
                    //    TerminalList.Remove(toDelete);
                    //    lbxMessage.Invoke((Action)(() => lbxMessage.Items.Insert(0, System.DateTime.Now + ".-. " + "Terminal Disconnected TerminalID :" + toDelete.TerminalCode + " Time : " + toDelete.ConnectionTime + " " + TerminalList.Count)));
                    //}
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        //private void EnrolUser(string EmpID, string EmpName, byte[] finger1, byte[] finger1Copy, byte[] finger2, byte[] finger2Copy, int TermID)
        private void EnrolUser(int TerminalID, int UserID, DateTime StartDate, DateTime EndDate, int EnrolFlag)
        {
            try
            {
                bizvirdiuser objEnrollment = new bizvirdiuser();
                objEnrollment.UserID = UserID;
                objEnrollment.SearchvirdiuserByID();
                if (objEnrollment.Dsvirdiuser.Tables[0].Rows.Count > 0)
                {
                    //EnrolUser(objEnrollment.Dsvirdiuser);
                    serveruserData.InitUserData();
                    serveruserData.UserID = Convert.ToInt32(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["userID"]);
                    serveruserData.UniqueID = Convert.ToString(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["userID"]);
                    serveruserData.UserName = Convert.ToString(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["userName"]);
                    serveruserData.IsAdmin = 0;
                    serveruserData.IsIdentify = 1;

                    serveruserData.AccessGroup = "1";

                    if (EnrolFlag == 1)//Enroll
                        serveruserData.SetAccessDate(1, StartDate.Year, StartDate.Month, StartDate.Day, EndDate.Year, EndDate.Month, EndDate.Day);//Allow
                    else if (EnrolFlag == 2)//Enrol or Restrict Access
                        serveruserData.SetAccessDate(2, StartDate.Year, StartDate.Month, StartDate.Day, EndDate.Year, EndDate.Month, EndDate.Day);//Restrict

                    if (Convert.ToString(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["rfidCard"]) != "")
                    {
                        serveruserData.AuthType = 4;//For Card refer UCSAPI_Type.h
                        serveruserData.SetCardData(1, Convert.ToString(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["rfidCard"]));
                        serveruserData.SetAuthType(0, 0, 0, 0, 1, 0);
                    }
                    else
                    {
                        serveruserData.IsFace1toN = 0;
                        serveruserData.AuthType = 1;//For FP refer UCSAPI_Type.h
                        serveruserData.SetAuthType(0, 1, 0, 0, 0, 0);
                        byte[] biFPData1_1 = new byte[500];
                        byte[] biFPData1_2 = new byte[500];
                        byte[] biFPData2_1 = new byte[500];
                        byte[] biFPData2_2 = new byte[500];

                        if (Convert.ToString(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["fingerOne"]) != "" && Convert.ToString(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["fingerOneCopy"]) != "")
                        {
                            biFPData1_1 = (byte[])objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["fingerOne"];
                            biFPData1_2 = (byte[])objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["fingerOneCopy"];
                        }

                        if (Convert.ToString(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["fingerTwo"]) != "" && Convert.ToString(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["fingerTwoCopy"]) != "")
                        {
                            biFPData2_1 = (byte[])objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["fingerTwo"];
                            biFPData2_2 = (byte[])objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["fingerTwoCopy"];
                        }

                        //serveruserData.AddFingerData((int)nFingerID1, (int)nTemplateType400, (byte[])Dsvirdiuser.Tables[0].Rows[0]["fingerOne"], (byte[])Dsvirdiuser.Tables[0].Rows[0]["fingerOneCopy"]);
                        if (Convert.ToString(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["fingerOne"]) != "" && Convert.ToString(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["fingerOneCopy"]) != "")
                            serveruserData.AddFingerData(0, (int)nTemplateType400, biFPData1_1, biFPData1_2);

                        if (Convert.ToString(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["fingerTwo"]) != "" && Convert.ToString(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["fingerTwoCopy"]) != "")
                            serveruserData.AddFingerData(1, (int)nTemplateType400, biFPData2_1, biFPData2_2);
                        serveruserData.SetDuressFinger(1, 1);
                    }

                    serveruserData.AddUserToTerminal(0, TerminalID, 1);

                    //if (Convert.ToInt32(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["userTypeID"]) == 2)
                    //{
                    //    
                    //    serveruserData.AddUserToTerminal(0, 3, 1);
                    //}
                    //else if (Convert.ToInt32(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["userTypeID"]) == 3)
                    //{
                    //    serveruserData.AddUserToTerminal(0, 2, 1);
                    //}
                    //else if (Convert.ToInt32(objEnrollment.Dsvirdiuser.Tables[0].Rows[0]["userTypeID"]) == 1)
                    //{

                    //}
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void SearchUser(int UserID)
        {
            try
            {
                //bizvirdiuser objEnrollment = new bizvirdiuser();
                //objEnrollment.UserID = UserID;
                //objEnrollment.SearchvirdiuserByID();
                //if (objEnrollment.Dsvirdiuser.Tables[0].Rows.Count > 0)
                //{
                //    EnrolUser(objEnrollment.Dsvirdiuser);
                //}
            }
            catch (Exception)
            {
            }

        }


        #endregion

        private void AutoUserEnrolment()
        {
            int GotError = 0;
            try
            {
                Thread.Sleep(30000);
                while (true)
                {
                    bizenrolment objEnrol = new bizenrolment();
                    objEnrol.Searchenrolment();

                    if (objEnrol.Dsenrolment.Tables[0].Rows.Count > 0)
                    {
                        //terminalUserData.GetUserDataFromTerminal(0, 4, Convert.ToInt32(objEnrol.Dsenrolment.Tables[0].Rows[0]["userID"]));//line No. 816

                        if (Convert.ToDateTime(objEnrol.Dsenrolment.Tables[0].Rows[0]["enrolDate"]) < Convert.ToDateTime(objEnrol.Dsenrolment.Tables[0].Rows[0]["deleteDate"]))
                        {
                            EnrolUser(Convert.ToInt32(objEnrol.Dsenrolment.Tables[0].Rows[0]["terminalID"]),
                           Convert.ToInt32(objEnrol.Dsenrolment.Tables[0].Rows[0]["userID"]),
                           Convert.ToDateTime(objEnrol.Dsenrolment.Tables[0].Rows[0]["enrolDate"]),
                           Convert.ToDateTime(objEnrol.Dsenrolment.Tables[0].Rows[0]["deleteDate"]),
                           Convert.ToInt32(objEnrol.Dsenrolment.Tables[0].Rows[0]["enrolFlag"]));

                            bizenrolment objEnr = new bizenrolment();
                            objEnr.TerminalID = Convert.ToInt32(objEnrol.Dsenrolment.Tables[0].Rows[0]["terminalID"]);
                            objEnr.UserID = Convert.ToInt32(objEnrol.Dsenrolment.Tables[0].Rows[0]["userID"]);
                            objEnr.TransMsg = "Reader Disconnected.";
                            objEnr.EnrolFlag = 5;//5=Sent to Delete
                            objEnr.UpdateenrolmentforFlag();
                            if (objEnr.Success == 1)
                            {

                            }
                        }
                        else
                        {
                            bizenrolment objEnr = new bizenrolment();
                            objEnr.TerminalID = Convert.ToInt32(objEnrol.Dsenrolment.Tables[0].Rows[0]["terminalID"]);
                            objEnr.UserID = Convert.ToInt32(objEnrol.Dsenrolment.Tables[0].Rows[0]["userID"]);
                            objEnr.TransMsg = "To date is less than from date.";
                            objEnr.EnrolFlag = 3;//3 To date is less than from date.
                            objEnr.UpdateenrolmentforFlag();
                            if (objEnr.Success == 1)
                            {
                                lbxMessage.Items.Insert(0, System.DateTime.Now + ".-." + "To date is less than from date. TerminalID : " + objEnr.TerminalID + " ClientID : 0 UserID : " + objEnr.UserID + " Code : " + this.ucsAPI.ErrorCode);
                                objLogGenerator.WriteToLog("To date is less than from date. TerminalID : " + objEnr.TerminalID + " ClientID : 0 UserID : " + objEnr.UserID + " Code : " + this.ucsAPI.ErrorCode);
                            }
                        }
                    }
                    else
                    {
                        bizenrolment objDelete = new bizenrolment();
                        objDelete.SearchenrolmentToDelete();
                        if (objDelete.Dsenrolment.Tables[0].Rows.Count > 0)
                        {
                            terminalUserData.DeleteUserFromTerminal(0, Convert.ToInt32(objDelete.Dsenrolment.Tables[0].Rows[0]["terminalID"]), Convert.ToInt32(objDelete.Dsenrolment.Tables[0].Rows[0]["userID"]));

                            bizenrolment objEnr = new bizenrolment();
                            objEnr.TerminalID = Convert.ToInt32(objDelete.Dsenrolment.Tables[0].Rows[0]["terminalID"]);
                            objEnr.UserID = Convert.ToInt32(objDelete.Dsenrolment.Tables[0].Rows[0]["userID"]);
                            objEnr.TransMsg = "Reader Disconnected.";
                            objEnr.EnrolFlag = 5;//5=Sent to Delete
                            objEnr.UpdateenrolmentforFlag();
                            if (objEnr.Success == 1)
                            {

                            }
                        }
                    }
                    Thread.Sleep(30000);
                }
            }
            catch (Exception ex)
            {
                GotError = 1;
                objLogGenerator.WriteToLog("Error at AutoUserEnrolment(): " + ex.Message);
            }
            finally
            {
                if (GotError == 1)
                    AutoUserEnrolment();
            }
        }

    }
}



