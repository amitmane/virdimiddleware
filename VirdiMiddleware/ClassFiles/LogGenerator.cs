﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace VirdiMiddleware
{
    class LogGenerator
    {
        static StreamWriter sw;
        public LogGenerator()
        {

        }

        public void CheckAndCreateTextFile()
        {
            try
            {
                string path = Application.StartupPath + "\\LogFiles\\" + "Log-" + System.DateTime.Now.ToShortDateString().Replace("/", "-") + ".txt";
                FileStream File1;

                if (!File.Exists(path))
                    File1 = new FileStream(path, FileMode.Create);
                else
                    File1 = new FileStream(path, FileMode.Append);

                sw = new StreamWriter(File1);
            }
            catch (Exception ex)
            {
                WriteToLog("Error in CheckAndCreateTextFile: " + ex.Message);
            }
        }

        public void WriteToLog(string data)
        {
            try
            {
                sw.WriteLine(System.DateTime.Now + " : " + data);
                sw.Flush();
            }
            catch (Exception ex)
            {

            }
        }

        public void CloseLog()
        {
            try
            {
                sw.Close();
            }
            catch (Exception ex)
            {

            }
        }
    }
}
