using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using Com.Rasilant.Database;

namespace Com.Rasilant.Services
{
	
	public class bizfilepath
	{
		/// <summary>
		/// Parameter name variables
		/// <summary>
		private string filePathIDName;
		private string filePathNameName;
		private string filePathName;
		private string readerIDName;
        private string terminalIDName;
		private string successName;
		
		/// <summary>
		/// Parameter type variables
		/// <summary>
		private MySqlDbType filePathIDType;
		private MySqlDbType filePathNameType;
		private MySqlDbType filePathType;
		private MySqlDbType readerIDType;
        private MySqlDbType terminalIDType;
		private MySqlDbType successType;
		
		/// <summary>
		/// filepath constructor
		/// <summary>
		public bizfilepath()
		{
			//
			// Parameter name variables initializations
			//
			filePathIDName = "P_filePathID";
			filePathNameName = "P_filePathName";
			filePathName = "P_filePath";
			readerIDName = "P_readerID";
            terminalIDName = "P_terminalID";
			successName = "P_success";
			
			//
			// Parameter type variables initialization
			//
			filePathIDType = MySqlDbType.Int32;
			filePathNameType = MySqlDbType.VarChar;
			filePathType = MySqlDbType.VarChar;
			readerIDType = MySqlDbType.Int32;
            terminalIDType = MySqlDbType.Int32;
			successType = MySqlDbType.Int32;
		}
		
		/// <summary>
		/// Gets or sets FilePathID
		/// <summary>
		private object mfilePathID;
		public object FilePathID
		{
			get
			{
				return mfilePathID;
			}
			set
			{
				mfilePathID = value;
			}
		}
		
		/// <summary>
		/// Gets or sets FilePathName
		/// <summary>
		private object mfilePathName;
		public object FilePathName
		{
			get
			{
				return mfilePathName;
			}
			set
			{
				mfilePathName = value;
			}
		}
		
		/// <summary>
		/// Gets or sets FilePath
		/// <summary>
		private object mfilePath;
		public object FilePath
		{
			get
			{
				return mfilePath;
			}
			set
			{
				mfilePath = value;
			}
		}
		
		/// <summary>
		/// Gets or sets ReaderID
		/// <summary>
		private object mreaderID;
		public object ReaderID
		{
			get
			{
				return mreaderID;
			}
			set
			{
				mreaderID = value;
			}
		}

        /// <summary>
        /// Gets or sets TerminalID
        /// <summary>
        private object mterminalID;
        public object TerminalID
        {
            get
            {
                return mterminalID;
            }
            set
            {
                mterminalID = value;
            }
        }

		/// <summary>
		/// Gets or sets result for insert or update records to database.
		/// <summary>
		private int msuccess;
		public int Success
		{
			get
			{
				return msuccess;
			}
			set
			{
				msuccess = value;
			}
		}
		
		/// <summary>
		/// Gets or sets filepath table
		/// <summary>
		private DataSet mdsfilepath;
		public DataSet Dsfilepath
		{
			get
			{
				return mdsfilepath;
			}
			set
			{
				mdsfilepath = value;
			}
		}
		
		/// <summary>
		/// Retrives data from database
		/// <summary>
		public void Searchfilepath()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspSearchfilepath";
				Dsfilepath = db.GetRecords();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Retrives data from database
		/// <summary>
		public void SearchfilepathByID()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspSearchfilepathByID";
		
				string[] parameterNames = new string[1];
				parameterNames[0] = filePathIDName;
				db.ParameterNames = parameterNames;
		
				MySqlDbType[] parameterTypes = new MySqlDbType[1];
				parameterTypes[0] = filePathIDType;
				db.ParameterTypes = parameterTypes;
		
				ParameterDirection[] parameterDirections = new ParameterDirection[1];
				parameterDirections[0] = ParameterDirection.Input;
				db.ParameterDirections = parameterDirections;
		
				object[] parameterValues = new object[1];
				parameterValues[0] = FilePathID;
				db.ParameterValues = parameterValues;
		
				Dsfilepath = db.GetRecords();
		
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        /// <summary>
        /// Retrives data from database
        /// <summary>
        public void SearchfilepathByTerminalID()
        {
            try
            {
                MySqlDataBaseConnections db = new MySqlDataBaseConnections();
                db.UserQueryType = QueryType.StoredProcedure;
                db.StoredProcedure = "uspSearchfilepathByTerminalID";

                string[] parameterNames = new string[1];
                parameterNames[0] = terminalIDName;
                db.ParameterNames = parameterNames;

                MySqlDbType[] parameterTypes = new MySqlDbType[1];
                parameterTypes[0] = terminalIDType;
                db.ParameterTypes = parameterTypes;

                ParameterDirection[] parameterDirections = new ParameterDirection[1];
                parameterDirections[0] = ParameterDirection.Input;
                db.ParameterDirections = parameterDirections;

                object[] parameterValues = new object[1];
                parameterValues[0] = TerminalID;
                db.ParameterValues = parameterValues;

                Dsfilepath = db.GetRecords();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
		

		/// <summary>
		/// Inserts data to database
		/// <summary>
		public void Insertfilepath()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspInsertfilepath";
		
				string[] parameterNames = new string[4];
				parameterNames[0] = filePathNameName;
				parameterNames[1] = filePathName;
				parameterNames[2] = readerIDName;
				parameterNames[3] = successName;
				db.ParameterNames = parameterNames;
		
				MySqlDbType[] parameterTypes = new MySqlDbType[4];
				parameterTypes[0] = filePathNameType;
				parameterTypes[1] = filePathType;
				parameterTypes[2] = readerIDType;
				parameterTypes[3] = successType;
				db.ParameterTypes = parameterTypes;
		
				ParameterDirection[] parameterDirections = new ParameterDirection[4];
				parameterDirections[0] = ParameterDirection.Input;
				parameterDirections[1] = ParameterDirection.Input;
				parameterDirections[2] = ParameterDirection.Input;
				parameterDirections[3] = ParameterDirection.Output;
				db.ParameterDirections = parameterDirections;
		
				object[] parameterValues = new object[4];
				parameterValues[0] = FilePathName;
				parameterValues[1] = FilePath;
				parameterValues[2] = ReaderID;
				parameterValues[3] = DBNull.Value;
				db.ParameterValues = parameterValues;
		
				db.InsertRecords();
				GetReturnValues(db);
		
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Updatess data to database
		/// <summary>
		public void Updatefilepath()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspUpdatefilepath";
		
				string[] parameterNames = new string[5];
				parameterNames[0] = filePathIDName;
				parameterNames[1] = filePathNameName;
				parameterNames[2] = filePathName;
				parameterNames[3] = readerIDName;
				parameterNames[4] = successName;
				db.ParameterNames = parameterNames;
		
				MySqlDbType[] parameterTypes = new MySqlDbType[5];
				parameterTypes[0] = filePathIDType;
				parameterTypes[1] = filePathNameType;
				parameterTypes[2] = filePathType;
				parameterTypes[3] = readerIDType;
				parameterTypes[4] = successType;
				db.ParameterTypes = parameterTypes;
		
				ParameterDirection[] parameterDirections = new ParameterDirection[5];
				parameterDirections[0] = ParameterDirection.Input;
				parameterDirections[1] = ParameterDirection.Input;
				parameterDirections[2] = ParameterDirection.Input;
				parameterDirections[3] = ParameterDirection.Input;
				parameterDirections[4] = ParameterDirection.Output;
				db.ParameterDirections = parameterDirections;
		
				object[] parameterValues = new object[5];
				parameterValues[0] = FilePathID;
				parameterValues[1] = FilePathName;
				parameterValues[2] = FilePath;
				parameterValues[3] = ReaderID;
				parameterValues[4] = DBNull.Value;
				db.ParameterValues = parameterValues;
		
				db.UpdateRecords();
				GetReturnValues(db);
		
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Deletes data from database
		/// <summary>
		public void Deletefilepath()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspDeletefilepath";
		
				string[] parameterNames = new string[1];
				parameterNames[0] = filePathIDName;
				db.ParameterNames = parameterNames;
		
				MySqlDbType[] parameterTypes = new MySqlDbType[1];
				parameterTypes[0] = filePathIDType;
				db.ParameterTypes = parameterTypes;
		
				ParameterDirection[] parameterDirections = new ParameterDirection[1];
				parameterDirections[0] = ParameterDirection.Input;
				db.ParameterDirections = parameterDirections;
		
				object[] parameterValues = new object[1];
				parameterValues[0] = FilePathID;
				db.ParameterValues = parameterValues;
		
				db.DeleteRecords();
				GetReturnValues(db);
		
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Gets the output valus from the stored procedure for the object db
		/// <summary>
		/// <param name="db">SqlDataBaseConnections object</param>
		private void GetReturnValues(MySqlDataBaseConnections db)
		{
			if (db.ReturnValues != null)
			{
				for (int i = 0; i < db.ReturnValues.Length / 2; i++)
				{
					if (((string)db.ReturnValues[i, 0]).CompareTo(successName) == 0)
						msuccess = (int)db.ReturnValues[i, 1];
				}
			}
		}
		
	}
}

