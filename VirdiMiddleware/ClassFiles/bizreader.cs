using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using Com.Rasilant.Database;

namespace Com.Rasilant.Services
{
	
	public class bizreader
	{
		/// <summary>
		/// Parameter name variables
		/// <summary>
		private string readerIDName;
		private string readerNameName;
		private string readerMACName;
		private string readerIPName;
		private string connStatusName;
		private string connTimeName;
		private string terminalIDName;
		private string successName;
		
		/// <summary>
		/// Parameter type variables
		/// <summary>
		private MySqlDbType readerIDType;
		private MySqlDbType readerNameType;
		private MySqlDbType readerMACType;
		private MySqlDbType readerIPType;
		private MySqlDbType connStatusType;
		private MySqlDbType connTimeType;
		private MySqlDbType terminalIDType;
		private MySqlDbType successType;
		
		/// <summary>
		/// reader constructor
		/// <summary>
		public bizreader()
		{
			//
			// Parameter name variables initializations
			//
			readerIDName = "P_readerID";
			readerNameName = "P_readerName";
			readerMACName = "P_readerMAC";
			readerIPName = "P_readerIP";
			connStatusName = "P_connStatus";
			connTimeName = "P_connTime";
			terminalIDName = "P_terminalID";
			successName = "P_success";
			
			//
			// Parameter type variables initialization
			//
			readerIDType = MySqlDbType.Int32;
			readerNameType = MySqlDbType.VarChar;
			readerMACType = MySqlDbType.VarChar;
			readerIPType = MySqlDbType.VarChar;
			connStatusType = MySqlDbType.Int32;
			connTimeType = MySqlDbType.DateTime;
			terminalIDType = MySqlDbType.Int32;
			successType = MySqlDbType.Int32;
		}
		
		/// <summary>
		/// Gets or sets ReaderID
		/// <summary>
		private object mreaderID;
		public object ReaderID
		{
			get
			{
				return mreaderID;
			}
			set
			{
				mreaderID = value;
			}
		}
		
		/// <summary>
		/// Gets or sets ReaderName
		/// <summary>
		private object mreaderName;
		public object ReaderName
		{
			get
			{
				return mreaderName;
			}
			set
			{
				mreaderName = value;
			}
		}
		
		/// <summary>
		/// Gets or sets ReaderMAC
		/// <summary>
		private object mreaderMAC;
		public object ReaderMAC
		{
			get
			{
				return mreaderMAC;
			}
			set
			{
				mreaderMAC = value;
			}
		}
		
		/// <summary>
		/// Gets or sets ReaderIP
		/// <summary>
		private object mreaderIP;
		public object ReaderIP
		{
			get
			{
				return mreaderIP;
			}
			set
			{
				mreaderIP = value;
			}
		}
		
		/// <summary>
		/// Gets or sets ConnStatus
		/// <summary>
		private object mconnStatus;
		public object ConnStatus
		{
			get
			{
				return mconnStatus;
			}
			set
			{
				mconnStatus = value;
			}
		}
		
		/// <summary>
		/// Gets or sets ConnTime
		/// <summary>
		private object mconnTime;
		public object ConnTime
		{
			get
			{
				return mconnTime;
			}
			set
			{
				mconnTime = value;
			}
		}
		
		/// <summary>
		/// Gets or sets TerminalID
		/// <summary>
		private object mterminalID;
		public object TerminalID
		{
			get
			{
				return mterminalID;
			}
			set
			{
				mterminalID = value;
			}
		}
		
		/// <summary>
		/// Gets or sets result for insert or update records to database.
		/// <summary>
		private int msuccess;
		public int Success
		{
			get
			{
				return msuccess;
			}
			set
			{
				msuccess = value;
			}
		}
		
		/// <summary>
		/// Gets or sets reader table
		/// <summary>
		private DataSet mdsreader;
		public DataSet Dsreader
		{
			get
			{
				return mdsreader;
			}
			set
			{
				mdsreader = value;
			}
		}
		
		/// <summary>
		/// Retrives data from database
		/// <summary>
		public void Searchreader()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspSearchreader";
				Dsreader = db.GetRecords();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Retrives data from database
		/// <summary>
		public void SearchreaderByID()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspSearchreaderByID";
		
				string[] parameterNames = new string[1];
				parameterNames[0] = readerIDName;
				db.ParameterNames = parameterNames;
		
				MySqlDbType[] parameterTypes = new MySqlDbType[1];
				parameterTypes[0] = readerIDType;
				db.ParameterTypes = parameterTypes;
		
				ParameterDirection[] parameterDirections = new ParameterDirection[1];
				parameterDirections[0] = ParameterDirection.Input;
				db.ParameterDirections = parameterDirections;
		
				object[] parameterValues = new object[1];
				parameterValues[0] = ReaderID;
				db.ParameterValues = parameterValues;
		
				Dsreader = db.GetRecords();
		
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Inserts data to database
		/// <summary>
		public void Insertreader()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspInsertreader";
		
				string[] parameterNames = new string[7];
				parameterNames[0] = readerNameName;
				parameterNames[1] = readerMACName;
				parameterNames[2] = readerIPName;
				parameterNames[3] = connStatusName;
				parameterNames[4] = connTimeName;
				parameterNames[5] = terminalIDName;
				parameterNames[6] = successName;
				db.ParameterNames = parameterNames;
		
				MySqlDbType[] parameterTypes = new MySqlDbType[7];
				parameterTypes[0] = readerNameType;
				parameterTypes[1] = readerMACType;
				parameterTypes[2] = readerIPType;
				parameterTypes[3] = connStatusType;
				parameterTypes[4] = connTimeType;
				parameterTypes[5] = terminalIDType;
				parameterTypes[6] = successType;
				db.ParameterTypes = parameterTypes;
		
				ParameterDirection[] parameterDirections = new ParameterDirection[7];
				parameterDirections[0] = ParameterDirection.Input;
				parameterDirections[1] = ParameterDirection.Input;
				parameterDirections[2] = ParameterDirection.Input;
				parameterDirections[3] = ParameterDirection.Input;
				parameterDirections[4] = ParameterDirection.Input;
				parameterDirections[5] = ParameterDirection.Input;
				parameterDirections[6] = ParameterDirection.Output;
				db.ParameterDirections = parameterDirections;
		
				object[] parameterValues = new object[7];
				parameterValues[0] = ReaderName;
				parameterValues[1] = ReaderMAC;
				parameterValues[2] = ReaderIP;
				parameterValues[3] = ConnStatus;
				parameterValues[4] = ConnTime;
				parameterValues[5] = TerminalID;
				parameterValues[6] = DBNull.Value;
				db.ParameterValues = parameterValues;
		
				db.InsertRecords();
				GetReturnValues(db);
		
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Updatess data to database
		/// <summary>
		public void Updatereader()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspUpdatereader";
		
				string[] parameterNames = new string[8];
				parameterNames[0] = readerIDName;
				parameterNames[1] = readerNameName;
				parameterNames[2] = readerMACName;
				parameterNames[3] = readerIPName;
				parameterNames[4] = connStatusName;
				parameterNames[5] = connTimeName;
				parameterNames[6] = terminalIDName;
				parameterNames[7] = successName;
				db.ParameterNames = parameterNames;
		
				MySqlDbType[] parameterTypes = new MySqlDbType[8];
				parameterTypes[0] = readerIDType;
				parameterTypes[1] = readerNameType;
				parameterTypes[2] = readerMACType;
				parameterTypes[3] = readerIPType;
				parameterTypes[4] = connStatusType;
				parameterTypes[5] = connTimeType;
				parameterTypes[6] = terminalIDType;
				parameterTypes[7] = successType;
				db.ParameterTypes = parameterTypes;
		
				ParameterDirection[] parameterDirections = new ParameterDirection[8];
				parameterDirections[0] = ParameterDirection.Input;
				parameterDirections[1] = ParameterDirection.Input;
				parameterDirections[2] = ParameterDirection.Input;
				parameterDirections[3] = ParameterDirection.Input;
				parameterDirections[4] = ParameterDirection.Input;
				parameterDirections[5] = ParameterDirection.Input;
				parameterDirections[6] = ParameterDirection.Input;
				parameterDirections[7] = ParameterDirection.Output;
				db.ParameterDirections = parameterDirections;
		
				object[] parameterValues = new object[8];
				parameterValues[0] = ReaderID;
				parameterValues[1] = ReaderName;
				parameterValues[2] = ReaderMAC;
				parameterValues[3] = ReaderIP;
				parameterValues[4] = ConnStatus;
				parameterValues[5] = ConnTime;
				parameterValues[6] = TerminalID;
				parameterValues[7] = DBNull.Value;
				db.ParameterValues = parameterValues;
		
				db.UpdateRecords();
				GetReturnValues(db);
		
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Deletes data from database
		/// <summary>
		public void Deletereader()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspDeletereader";
		
				string[] parameterNames = new string[1];
				parameterNames[0] = readerIDName;
				db.ParameterNames = parameterNames;
		
				MySqlDbType[] parameterTypes = new MySqlDbType[1];
				parameterTypes[0] = readerIDType;
				db.ParameterTypes = parameterTypes;
		
				ParameterDirection[] parameterDirections = new ParameterDirection[1];
				parameterDirections[0] = ParameterDirection.Input;
				db.ParameterDirections = parameterDirections;
		
				object[] parameterValues = new object[1];
				parameterValues[0] = ReaderID;
				db.ParameterValues = parameterValues;
		
				db.DeleteRecords();
				GetReturnValues(db);
		
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Gets the output valus from the stored procedure for the object db
		/// <summary>
		/// <param name="db">SqlDataBaseConnections object</param>
		private void GetReturnValues(MySqlDataBaseConnections db)
		{
			if (db.ReturnValues != null)
			{
				for (int i = 0; i < db.ReturnValues.Length / 2; i++)
				{
					if (((string)db.ReturnValues[i, 0]).CompareTo(successName) == 0)
						msuccess = (int)db.ReturnValues[i, 1];
				}
			}
		}
		
	}
}

