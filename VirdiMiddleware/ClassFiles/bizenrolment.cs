using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using Com.Rasilant.Database;

namespace Com.Rasilant.Services
{
	
	public class bizenrolment
	{
        /// <summary>
        /// Parameter name variables
        /// <summary>
        private string enrolmentIDName;
        private string clientIDName;
        private string terminalIDName;
        private string userIDName;
        private string enrolDateName;
        private string deleteDateName;
        private string enrolFlagName;
        private string transDateName;
        private string transMsgName;
        private string successName;

        /// <summary>
        /// Parameter type variables
        /// <summary>
        private MySqlDbType enrolmentIDType;
        private MySqlDbType clientIDType;
        private MySqlDbType terminalIDType;
        private MySqlDbType userIDType;
        private MySqlDbType enrolDateType;
        private MySqlDbType deleteDateType;
        private MySqlDbType enrolFlagType;
        private MySqlDbType transDateType;
        private MySqlDbType transMsgType;
        private MySqlDbType successType;

        /// <summary>
        /// enrolment constructor
        /// <summary>
        public bizenrolment()
        {
            //
            // Parameter name variables initializations
            //
            enrolmentIDName = "P_enrolmentID";
            clientIDName = "P_clientID";
            terminalIDName = "P_terminalID";
            userIDName = "P_userID";
            enrolDateName = "P_enrolDate";
            deleteDateName = "P_deleteDate";
            enrolFlagName = "P_enrolFlag";
            transDateName = "P_transDate";
            transMsgName = "P_transMsg";
            successName = "P_success";

            //
            // Parameter type variables initialization
            //
            enrolmentIDType = MySqlDbType.Int32;
            clientIDType = MySqlDbType.Int32;
            terminalIDType = MySqlDbType.Int32;
            userIDType = MySqlDbType.Int32;
            enrolDateType = MySqlDbType.DateTime;
            deleteDateType = MySqlDbType.DateTime;
            enrolFlagType = MySqlDbType.Int32;
            transDateType = MySqlDbType.DateTime;
            transMsgType = MySqlDbType.VarChar;
            successType = MySqlDbType.Int32;
        }

        /// <summary>
        /// Gets or sets EnrolmentID
        /// <summary>
        private object menrolmentID;
        public object EnrolmentID
        {
            get
            {
                return menrolmentID;
            }
            set
            {
                menrolmentID = value;
            }
        }

        /// <summary>
        /// Gets or sets ClientID
        /// <summary>
        private object mclientID;
        public object ClientID
        {
            get
            {
                return mclientID;
            }
            set
            {
                mclientID = value;
            }
        }

        /// <summary>
        /// Gets or sets TerminalID
        /// <summary>
        private object mterminalID;
        public object TerminalID
        {
            get
            {
                return mterminalID;
            }
            set
            {
                mterminalID = value;
            }
        }

        /// <summary>
        /// Gets or sets UserID
        /// <summary>
        private object muserID;
        public object UserID
        {
            get
            {
                return muserID;
            }
            set
            {
                muserID = value;
            }
        }

        /// <summary>
        /// Gets or sets EnrolDate
        /// <summary>
        private object menrolDate;
        public object EnrolDate
        {
            get
            {
                return menrolDate;
            }
            set
            {
                menrolDate = value;
            }
        }

        /// <summary>
        /// Gets or sets DeleteDate
        /// <summary>
        private object mdeleteDate;
        public object DeleteDate
        {
            get
            {
                return mdeleteDate;
            }
            set
            {
                mdeleteDate = value;
            }
        }

        /// <summary>
        /// Gets or sets EnrolFlag
        /// <summary>
        private object menrolFlag;
        public object EnrolFlag
        {
            get
            {
                return menrolFlag;
            }
            set
            {
                menrolFlag = value;
            }
        }

        /// <summary>
        /// Gets or sets TransDate
        /// <summary>
        private object mtransDate;
        public object TransDate
        {
            get
            {
                return mtransDate;
            }
            set
            {
                mtransDate = value;
            }
        }

        /// <summary>
        /// Gets or sets TransMsg
        /// <summary>
        private object mtransMsg;
        public object TransMsg
        {
            get
            {
                return mtransMsg;
            }
            set
            {
                mtransMsg = value;
            }
        }

        /// <summary>
        /// Gets or sets result for insert or update records to database.
        /// <summary>
        private int msuccess;
        public int Success
        {
            get
            {
                return msuccess;
            }
            set
            {
                msuccess = value;
            }
        }

        /// <summary>
        /// Gets or sets enrolment table
        /// <summary>
        private DataSet mdsenrolment;
        public DataSet Dsenrolment
        {
            get
            {
                return mdsenrolment;
            }
            set
            {
                mdsenrolment = value;
            }
        }
		
		/// <summary>
		/// Retrives data from database
		/// <summary>
		public void Searchenrolment()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspSearchenrolment";
				Dsenrolment = db.GetRecords();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        /// <summary>
        /// Retrives data from database
        /// <summary>
        public void SearchenrolmentToDelete()
        {
            try
            {
                MySqlDataBaseConnections db = new MySqlDataBaseConnections();
                db.UserQueryType = QueryType.StoredProcedure;
                db.StoredProcedure = "uspSearchenrolmentToDelete";
                Dsenrolment = db.GetRecords();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
		
		/// <summary>
		/// Retrives data from database
		/// <summary>
		public void SearchenrolmentByID()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspSearchenrolmentByID";
		
				string[] parameterNames = new string[1];
				parameterNames[0] = enrolmentIDName;
				db.ParameterNames = parameterNames;
		
				MySqlDbType[] parameterTypes = new MySqlDbType[1];
				parameterTypes[0] = enrolmentIDType;
				db.ParameterTypes = parameterTypes;
		
				ParameterDirection[] parameterDirections = new ParameterDirection[1];
				parameterDirections[0] = ParameterDirection.Input;
				db.ParameterDirections = parameterDirections;
		
				object[] parameterValues = new object[1];
				parameterValues[0] = EnrolmentID;
				db.ParameterValues = parameterValues;
		
				Dsenrolment = db.GetRecords();
		
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Inserts data to database
		/// <summary>
		public void Insertenrolment()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspInsertenrolment";
		
				string[] parameterNames = new string[8];
				parameterNames[0] = clientIDName;
				parameterNames[1] = terminalIDName;
				parameterNames[2] = userIDName;
				parameterNames[3] = enrolDateName;
				parameterNames[4] = deleteDateName;
				parameterNames[5] = enrolFlagName;
				parameterNames[6] = transDateName;
				parameterNames[7] = successName;
				db.ParameterNames = parameterNames;
		
				MySqlDbType[] parameterTypes = new MySqlDbType[8];
				parameterTypes[0] = clientIDType;
				parameterTypes[1] = terminalIDType;
				parameterTypes[2] = userIDType;
				parameterTypes[3] = enrolDateType;
				parameterTypes[4] = deleteDateType;
				parameterTypes[5] = enrolFlagType;
				parameterTypes[6] = transDateType;
				parameterTypes[7] = successType;
				db.ParameterTypes = parameterTypes;
		
				ParameterDirection[] parameterDirections = new ParameterDirection[8];
				parameterDirections[0] = ParameterDirection.Input;
				parameterDirections[1] = ParameterDirection.Input;
				parameterDirections[2] = ParameterDirection.Input;
				parameterDirections[3] = ParameterDirection.Input;
				parameterDirections[4] = ParameterDirection.Input;
				parameterDirections[5] = ParameterDirection.Input;
				parameterDirections[6] = ParameterDirection.Input;
				parameterDirections[7] = ParameterDirection.Output;
				db.ParameterDirections = parameterDirections;
		
				object[] parameterValues = new object[8];
				parameterValues[0] = ClientID;
				parameterValues[1] = TerminalID;
				parameterValues[2] = UserID;
				parameterValues[3] = EnrolDate;
				parameterValues[4] = DeleteDate;
				parameterValues[5] = EnrolFlag;
				parameterValues[6] = TransDate;
				parameterValues[7] = DBNull.Value;
				db.ParameterValues = parameterValues;
		
				db.InsertRecords();
				GetReturnValues(db);
		
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Updatess data to database
		/// <summary>
		public void Updateenrolment()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspUpdateenrolment";
		
				string[] parameterNames = new string[9];
				parameterNames[0] = enrolmentIDName;
				parameterNames[1] = clientIDName;
				parameterNames[2] = terminalIDName;
				parameterNames[3] = userIDName;
				parameterNames[4] = enrolDateName;
				parameterNames[5] = deleteDateName;
				parameterNames[6] = enrolFlagName;
				parameterNames[7] = transDateName;
				parameterNames[8] = successName;
				db.ParameterNames = parameterNames;
		
				MySqlDbType[] parameterTypes = new MySqlDbType[9];
				parameterTypes[0] = enrolmentIDType;
				parameterTypes[1] = clientIDType;
				parameterTypes[2] = terminalIDType;
				parameterTypes[3] = userIDType;
				parameterTypes[4] = enrolDateType;
				parameterTypes[5] = deleteDateType;
				parameterTypes[6] = enrolFlagType;
				parameterTypes[7] = transDateType;
				parameterTypes[8] = successType;
				db.ParameterTypes = parameterTypes;
		
				ParameterDirection[] parameterDirections = new ParameterDirection[9];
				parameterDirections[0] = ParameterDirection.Input;
				parameterDirections[1] = ParameterDirection.Input;
				parameterDirections[2] = ParameterDirection.Input;
				parameterDirections[3] = ParameterDirection.Input;
				parameterDirections[4] = ParameterDirection.Input;
				parameterDirections[5] = ParameterDirection.Input;
				parameterDirections[6] = ParameterDirection.Input;
				parameterDirections[7] = ParameterDirection.Input;
				parameterDirections[8] = ParameterDirection.Output;
				db.ParameterDirections = parameterDirections;
		
				object[] parameterValues = new object[9];
				parameterValues[0] = EnrolmentID;
				parameterValues[1] = ClientID;
				parameterValues[2] = TerminalID;
				parameterValues[3] = UserID;
				parameterValues[4] = EnrolDate;
				parameterValues[5] = DeleteDate;
				parameterValues[6] = EnrolFlag;
				parameterValues[7] = TransDate;
				parameterValues[8] = DBNull.Value;
				db.ParameterValues = parameterValues;
		
				db.UpdateRecords();
				GetReturnValues(db);
		
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        
        /// <summary>
        /// Updatess data to database
        /// <summary>
        public void UpdateenrolmentforFlag()
        {
            try
            {
                MySqlDataBaseConnections db = new MySqlDataBaseConnections();
                db.UserQueryType = QueryType.StoredProcedure;
                db.StoredProcedure = "uspUpdateenrolmentforFlag";

                string[] parameterNames = new string[5];
                parameterNames[0] = terminalIDName;
                parameterNames[1] = userIDName;
                parameterNames[2] = enrolFlagName;
                parameterNames[3] = transMsgName;
                parameterNames[4] = successName;
                db.ParameterNames = parameterNames;

                MySqlDbType[] parameterTypes = new MySqlDbType[5];
                parameterTypes[0] = terminalIDType;
                parameterTypes[1] = userIDType;
                parameterTypes[2] = enrolFlagType;
                parameterTypes[3] = transMsgType;
                parameterTypes[4] = successType;
                db.ParameterTypes = parameterTypes;

                ParameterDirection[] parameterDirections = new ParameterDirection[5];
                parameterDirections[0] = ParameterDirection.Input;
                parameterDirections[1] = ParameterDirection.Input;
                parameterDirections[2] = ParameterDirection.Input;
                parameterDirections[3] = ParameterDirection.Input;
                parameterDirections[4] = ParameterDirection.Output;
                db.ParameterDirections = parameterDirections;

                object[] parameterValues = new object[5];
                parameterValues[0] = TerminalID;
                parameterValues[1] = UserID;
                parameterValues[2] = EnrolFlag;
                parameterValues[3] = TransMsg;
                parameterValues[4] = DBNull.Value;
                db.ParameterValues = parameterValues;

                db.UpdateRecords();
                GetReturnValues(db);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Updatess data to database
        /// <summary>
        public void UpdateenrolmentforFlagMsg()
        {
            try
            {
                MySqlDataBaseConnections db = new MySqlDataBaseConnections();
                db.UserQueryType = QueryType.StoredProcedure;
                db.StoredProcedure = "uspUpdateenrolmentforFlagMsg";

                string[] parameterNames = new string[4];
                parameterNames[0] = userIDName;
                parameterNames[1] = enrolFlagName;
                parameterNames[2] = transMsgName;
                parameterNames[3] = successName;
                db.ParameterNames = parameterNames;

                MySqlDbType[] parameterTypes = new MySqlDbType[4];
                parameterTypes[0] = userIDType;
                parameterTypes[1] = enrolFlagType;
                parameterTypes[2] = transMsgType;
                parameterTypes[3] = successType;
                db.ParameterTypes = parameterTypes;

                ParameterDirection[] parameterDirections = new ParameterDirection[4];
                parameterDirections[0] = ParameterDirection.Input;
                parameterDirections[1] = ParameterDirection.Input;
                parameterDirections[2] = ParameterDirection.Input;
                parameterDirections[3] = ParameterDirection.Output;
                db.ParameterDirections = parameterDirections;

                object[] parameterValues = new object[4];
                parameterValues[0] = UserID;
                parameterValues[1] = EnrolFlag;
                parameterValues[2] = TransMsg;
                parameterValues[3] = DBNull.Value;
                db.ParameterValues = parameterValues;

                db.UpdateRecords();
                GetReturnValues(db);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
		
		/// <summary>
		/// Deletes data from database
		/// <summary>
		public void Deleteenrolment()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspDeleteenrolment";
		
				string[] parameterNames = new string[1];
				parameterNames[0] = enrolmentIDName;
				db.ParameterNames = parameterNames;
		
				MySqlDbType[] parameterTypes = new MySqlDbType[1];
				parameterTypes[0] = enrolmentIDType;
				db.ParameterTypes = parameterTypes;
		
				ParameterDirection[] parameterDirections = new ParameterDirection[1];
				parameterDirections[0] = ParameterDirection.Input;
				db.ParameterDirections = parameterDirections;
		
				object[] parameterValues = new object[1];
				parameterValues[0] = EnrolmentID;
				db.ParameterValues = parameterValues;
		
				db.DeleteRecords();
				GetReturnValues(db);
		
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Gets the output valus from the stored procedure for the object db
		/// <summary>
		/// <param name="db">SqlDataBaseConnections object</param>
		private void GetReturnValues(MySqlDataBaseConnections db)
		{
			if (db.ReturnValues != null)
			{
				for (int i = 0; i < db.ReturnValues.Length / 2; i++)
				{
					if (((string)db.ReturnValues[i, 0]).CompareTo(successName) == 0)
						msuccess = (int)db.ReturnValues[i, 1];
				}
			}
		}
		
	}
}

