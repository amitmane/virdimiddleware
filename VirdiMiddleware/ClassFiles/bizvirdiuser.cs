using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using Com.Rasilant.Database;

namespace Com.Rasilant.Services
{
	
	public class bizvirdiuser
	{
        /// <summary>
        /// Parameter name variables
        /// <summary>
        private string userIDName;
        private string userNameName;
        private string userCodeName;
        private string userTypeIDName;
        private string deptIDName;
        private string desigIDName;
        private string oTLNumberName;
        private string drivingLicenseNoName;
        private string drivingLicenseValidName;
        private string validityHazEndorName;
        private string day3PCVOCertNoName;
        private string day3PCVOCertDateName;
        private string day3PCVOValidName;
        private string day1PCVOCertNoName;
        private string day1PCVOCertDateName;
        private string day1PCVOValidName;
        private string policeVerificationName;
        private string fingerOneName;
        private string fingerOneCopyName;
        private string fingerTwoName;
        private string fingerTwoCopyName;
        private string rfidCardName;
        private string userPhotoName;
        private string createdDateName;
        private string successName;

        /// <summary>
        /// Parameter type variables
        /// <summary>
        private MySqlDbType userIDType;
        private MySqlDbType userNameType;
        private MySqlDbType userCodeType;
        private MySqlDbType userTypeIDType;
        private MySqlDbType deptIDType;
        private MySqlDbType desigIDType;
        private MySqlDbType oTLNumberType;
        private MySqlDbType drivingLicenseNoType;
        private MySqlDbType drivingLicenseValidType;
        private MySqlDbType validityHazEndorType;
        private MySqlDbType day3PCVOCertNoType;
        private MySqlDbType day3PCVOCertDateType;
        private MySqlDbType day3PCVOValidType;
        private MySqlDbType day1PCVOCertNoType;
        private MySqlDbType day1PCVOCertDateType;
        private MySqlDbType day1PCVOValidType;
        private MySqlDbType policeVerificationType;
        private MySqlDbType fingerOneType;
        private MySqlDbType fingerOneCopyType;
        private MySqlDbType fingerTwoType;
        private MySqlDbType fingerTwoCopyType;
        private MySqlDbType rfidCardType;
        private MySqlDbType userPhotoType;
        private MySqlDbType createdDateType;
        private MySqlDbType successType;

        /// <summary>
        /// virdiuser constructor
        /// <summary>
        public bizvirdiuser()
		{
			//
			// Parameter name variables initializations
			//
			userIDName = "P_userID";
			userNameName = "P_userName";
			userCodeName = "P_userCode";
			userTypeIDName = "P_userTypeID";
			deptIDName = "P_deptID";
			desigIDName = "P_desigID";
			oTLNumberName = "P_oTLNumber";
			drivingLicenseNoName = "P_drivingLicenseNo";
			drivingLicenseValidName = "P_drivingLicenseValid";
			validityHazEndorName = "P_validityHazEndor";
			day3PCVOCertNoName = "P_day3PCVOCertNo";
			day3PCVOCertDateName = "P_day3PCVOCertDate";
			day3PCVOValidName = "P_day3PCVOValid";
			day1PCVOCertNoName = "P_day1PCVOCertNo";
			day1PCVOCertDateName = "P_day1PCVOCertDate";
			day1PCVOValidName = "P_day1PCVOValid";
			policeVerificationName = "P_policeVerification";
			fingerOneName = "P_fingerOne";
			fingerOneCopyName = "P_fingerOneCopy";
			fingerTwoName = "P_fingerTwo";
			fingerTwoCopyName = "P_fingerTwoCopy";
			rfidCardName = "P_rfidCard";
			userPhotoName = "P_userPhoto";
			createdDateName = "P_createdDate";
			successName = "P_success";
			
			//
			// Parameter type variables initialization
			//
			userIDType = MySqlDbType.Int32;
			userNameType = MySqlDbType.VarChar;
			userCodeType = MySqlDbType.VarChar;
			userTypeIDType = MySqlDbType.Int32;
			deptIDType = MySqlDbType.Int32;
			desigIDType = MySqlDbType.Int32;
			oTLNumberType = MySqlDbType.VarChar;
			drivingLicenseNoType = MySqlDbType.VarChar;
			drivingLicenseValidType = MySqlDbType.DateTime;
			validityHazEndorType = MySqlDbType.DateTime;
			day3PCVOCertNoType = MySqlDbType.VarChar;
			day3PCVOCertDateType = MySqlDbType.DateTime;
			day3PCVOValidType = MySqlDbType.DateTime;
			day1PCVOCertNoType = MySqlDbType.VarChar;
			day1PCVOCertDateType = MySqlDbType.DateTime;
			day1PCVOValidType = MySqlDbType.DateTime;
			policeVerificationType = MySqlDbType.Int32;
			fingerOneType = MySqlDbType.MediumBlob;
			fingerOneCopyType = MySqlDbType.MediumBlob;
			fingerTwoType = MySqlDbType.MediumBlob;
			fingerTwoCopyType = MySqlDbType.MediumBlob;
			rfidCardType = MySqlDbType.VarChar;
            userPhotoType = MySqlDbType.LongBlob;
			createdDateType = MySqlDbType.DateTime;
			successType = MySqlDbType.Int32;
		}

        /// <summary>
        /// Gets or sets UserID
        /// <summary>
        private object muserID;
        public object UserID
        {
            get
            {
                return muserID;
            }
            set
            {
                muserID = value;
            }
        }

        /// <summary>
        /// Gets or sets UserName
        /// <summary>
        private object muserName;
        public object UserName
        {
            get
            {
                return muserName;
            }
            set
            {
                muserName = value;
            }
        }

        /// <summary>
        /// Gets or sets UserCode
        /// <summary>
        private object muserCode;
        public object UserCode
        {
            get
            {
                return muserCode;
            }
            set
            {
                muserCode = value;
            }
        }

        /// <summary>
        /// Gets or sets UserTypeID
        /// <summary>
        private object muserTypeID;
        public object UserTypeID
        {
            get
            {
                return muserTypeID;
            }
            set
            {
                muserTypeID = value;
            }
        }

        /// <summary>
        /// Gets or sets DeptID
        /// <summary>
        private object mdeptID;
        public object DeptID
        {
            get
            {
                return mdeptID;
            }
            set
            {
                mdeptID = value;
            }
        }

        /// <summary>
        /// Gets or sets DesigID
        /// <summary>
        private object mdesigID;
        public object DesigID
        {
            get
            {
                return mdesigID;
            }
            set
            {
                mdesigID = value;
            }
        }

        /// <summary>
        /// Gets or sets OTLNumber
        /// <summary>
        private object moTLNumber;
        public object OTLNumber
        {
            get
            {
                return moTLNumber;
            }
            set
            {
                moTLNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets DrivingLicenseNo
        /// <summary>
        private object mdrivingLicenseNo;
        public object DrivingLicenseNo
        {
            get
            {
                return mdrivingLicenseNo;
            }
            set
            {
                mdrivingLicenseNo = value;
            }
        }

        /// <summary>
        /// Gets or sets DrivingLicenseValid
        /// <summary>
        private object mdrivingLicenseValid;
        public object DrivingLicenseValid
        {
            get
            {
                return mdrivingLicenseValid;
            }
            set
            {
                mdrivingLicenseValid = value;
            }
        }

        /// <summary>
        /// Gets or sets ValidityHazEndor
        /// <summary>
        private object mvalidityHazEndor;
        public object ValidityHazEndor
        {
            get
            {
                return mvalidityHazEndor;
            }
            set
            {
                mvalidityHazEndor = value;
            }
        }

        /// <summary>
        /// Gets or sets Day3PCVOCertNo
        /// <summary>
        private object mday3PCVOCertNo;
        public object Day3PCVOCertNo
        {
            get
            {
                return mday3PCVOCertNo;
            }
            set
            {
                mday3PCVOCertNo = value;
            }
        }

        /// <summary>
        /// Gets or sets Day3PCVOCertDate
        /// <summary>
        private object mday3PCVOCertDate;
        public object Day3PCVOCertDate
        {
            get
            {
                return mday3PCVOCertDate;
            }
            set
            {
                mday3PCVOCertDate = value;
            }
        }

        /// <summary>
        /// Gets or sets Day3PCVOValid
        /// <summary>
        private object mday3PCVOValid;
        public object Day3PCVOValid
        {
            get
            {
                return mday3PCVOValid;
            }
            set
            {
                mday3PCVOValid = value;
            }
        }

        /// <summary>
        /// Gets or sets Day1PCVOCertNo
        /// <summary>
        private object mday1PCVOCertNo;
        public object Day1PCVOCertNo
        {
            get
            {
                return mday1PCVOCertNo;
            }
            set
            {
                mday1PCVOCertNo = value;
            }
        }

        /// <summary>
        /// Gets or sets Day1PCVOCertDate
        /// <summary>
        private object mday1PCVOCertDate;
        public object Day1PCVOCertDate
        {
            get
            {
                return mday1PCVOCertDate;
            }
            set
            {
                mday1PCVOCertDate = value;
            }
        }

        /// <summary>
        /// Gets or sets Day1PCVOValid
        /// <summary>
        private object mday1PCVOValid;
        public object Day1PCVOValid
        {
            get
            {
                return mday1PCVOValid;
            }
            set
            {
                mday1PCVOValid = value;
            }
        }

        /// <summary>
        /// Gets or sets PoliceVerification
        /// <summary>
        private object mpoliceVerification;
        public object PoliceVerification
        {
            get
            {
                return mpoliceVerification;
            }
            set
            {
                mpoliceVerification = value;
            }
        }

        /// <summary>
        /// Gets or sets FingerOne
        /// <summary>
        private object mfingerOne;
        public object FingerOne
        {
            get
            {
                return mfingerOne;
            }
            set
            {
                mfingerOne = value;
            }
        }

        /// <summary>
        /// Gets or sets FingerOneCopy
        /// <summary>
        private object mfingerOneCopy;
        public object FingerOneCopy
        {
            get
            {
                return mfingerOneCopy;
            }
            set
            {
                mfingerOneCopy = value;
            }
        }

        /// <summary>
        /// Gets or sets FingerTwo
        /// <summary>
        private object mfingerTwo;
        public object FingerTwo
        {
            get
            {
                return mfingerTwo;
            }
            set
            {
                mfingerTwo = value;
            }
        }

        /// <summary>
        /// Gets or sets FingerTwoCopy
        /// <summary>
        private object mfingerTwoCopy;
        public object FingerTwoCopy
        {
            get
            {
                return mfingerTwoCopy;
            }
            set
            {
                mfingerTwoCopy = value;
            }
        }

        /// <summary>
        /// Gets or sets RfidCard
        /// <summary>
        private object mrfidCard;
        public object RfidCard
        {
            get
            {
                return mrfidCard;
            }
            set
            {
                mrfidCard = value;
            }
        }

        /// <summary>
        /// Gets or sets UserPhoto
        /// <summary>
        private object muserPhoto;
        public object UserPhoto
        {
            get
            {
                return muserPhoto;
            }
            set
            {
                muserPhoto = value;
            }
        }

        /// <summary>
        /// Gets or sets CreatedDate
        /// <summary>
        private object mcreatedDate;
        public object CreatedDate
        {
            get
            {
                return mcreatedDate;
            }
            set
            {
                mcreatedDate = value;
            }
        }

        /// <summary>
        /// Gets or sets result for insert or update records to database.
        /// <summary>
        private int msuccess;
        public int Success
        {
            get
            {
                return msuccess;
            }
            set
            {
                msuccess = value;
            }
        }

        /// <summary>
        /// Gets or sets virdiuser table
        /// <summary>
        private DataSet mdsvirdiuser;
        public DataSet Dsvirdiuser
        {
            get
            {
                return mdsvirdiuser;
            }
            set
            {
                mdsvirdiuser = value;
            }
        }
		
		/// <summary>
		/// Retrives data from database
		/// <summary>
		public void Searchvirdiuser()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspSearchvirdiuser";
				Dsvirdiuser = db.GetRecords();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        /// <summary>
        /// Retrives data from database for Enrolment
        /// <summary>
        public void SearchEnrolment()
        {
            try
            {
                MySqlDataBaseConnections db = new MySqlDataBaseConnections();
                db.UserQueryType = QueryType.StoredProcedure;
                db.StoredProcedure = "uspSearchEnrolment";
                Dsvirdiuser = db.GetRecords();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
		
		/// <summary>
		/// Retrives data from database
		/// <summary>
		public void SearchvirdiuserByID()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspSearchvirdiuserByID";
		
				string[] parameterNames = new string[1];
				parameterNames[0] = userIDName;
				db.ParameterNames = parameterNames;
		
				MySqlDbType[] parameterTypes = new MySqlDbType[1];
				parameterTypes[0] = userIDType;
				db.ParameterTypes = parameterTypes;
		
				ParameterDirection[] parameterDirections = new ParameterDirection[1];
				parameterDirections[0] = ParameterDirection.Input;
				db.ParameterDirections = parameterDirections;
		
				object[] parameterValues = new object[1];
				parameterValues[0] = UserID;
				db.ParameterValues = parameterValues;
		
				Dsvirdiuser = db.GetRecords();
		
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        /// <summary>
        /// Inserts data to database
        /// <summary>
        public void Insertvirdiuser()
        {
            try
            {
                MySqlDataBaseConnections db = new MySqlDataBaseConnections();
                db.UserQueryType = QueryType.StoredProcedure;
                db.StoredProcedure = "uspInsertvirdiuser";

                string[] parameterNames = new string[24];
                parameterNames[0] = userNameName;
                parameterNames[1] = userCodeName;
                parameterNames[2] = userTypeIDName;
                parameterNames[3] = deptIDName;
                parameterNames[4] = desigIDName;
                parameterNames[5] = oTLNumberName;
                parameterNames[6] = drivingLicenseNoName;
                parameterNames[7] = drivingLicenseValidName;
                parameterNames[8] = validityHazEndorName;
                parameterNames[9] = day3PCVOCertNoName;
                parameterNames[10] = day3PCVOCertDateName;
                parameterNames[11] = day3PCVOValidName;
                parameterNames[12] = day1PCVOCertNoName;
                parameterNames[13] = day1PCVOCertDateName;
                parameterNames[14] = day1PCVOValidName;
                parameterNames[15] = policeVerificationName;
                parameterNames[16] = fingerOneName;
                parameterNames[17] = fingerOneCopyName;
                parameterNames[18] = fingerTwoName;
                parameterNames[19] = fingerTwoCopyName;
                parameterNames[20] = rfidCardName;
                parameterNames[21] = userPhotoName;
                parameterNames[22] = createdDateName;
                parameterNames[23] = successName;
                db.ParameterNames = parameterNames;

                MySqlDbType[] parameterTypes = new MySqlDbType[24];
                parameterTypes[0] = userNameType;
                parameterTypes[1] = userCodeType;
                parameterTypes[2] = userTypeIDType;
                parameterTypes[3] = deptIDType;
                parameterTypes[4] = desigIDType;
                parameterTypes[5] = oTLNumberType;
                parameterTypes[6] = drivingLicenseNoType;
                parameterTypes[7] = drivingLicenseValidType;
                parameterTypes[8] = validityHazEndorType;
                parameterTypes[9] = day3PCVOCertNoType;
                parameterTypes[10] = day3PCVOCertDateType;
                parameterTypes[11] = day3PCVOValidType;
                parameterTypes[12] = day1PCVOCertNoType;
                parameterTypes[13] = day1PCVOCertDateType;
                parameterTypes[14] = day1PCVOValidType;
                parameterTypes[15] = policeVerificationType;
                parameterTypes[16] = fingerOneType;
                parameterTypes[17] = fingerOneCopyType;
                parameterTypes[18] = fingerTwoType;
                parameterTypes[19] = fingerTwoCopyType;
                parameterTypes[20] = rfidCardType;
                parameterTypes[21] = userPhotoType;
                parameterTypes[22] = createdDateType;
                parameterTypes[23] = successType;
                db.ParameterTypes = parameterTypes;

                ParameterDirection[] parameterDirections = new ParameterDirection[24];
                parameterDirections[0] = ParameterDirection.Input;
                parameterDirections[1] = ParameterDirection.Input;
                parameterDirections[2] = ParameterDirection.Input;
                parameterDirections[3] = ParameterDirection.Input;
                parameterDirections[4] = ParameterDirection.Input;
                parameterDirections[5] = ParameterDirection.Input;
                parameterDirections[6] = ParameterDirection.Input;
                parameterDirections[7] = ParameterDirection.Input;
                parameterDirections[8] = ParameterDirection.Input;
                parameterDirections[9] = ParameterDirection.Input;
                parameterDirections[10] = ParameterDirection.Input;
                parameterDirections[11] = ParameterDirection.Input;
                parameterDirections[12] = ParameterDirection.Input;
                parameterDirections[13] = ParameterDirection.Input;
                parameterDirections[14] = ParameterDirection.Input;
                parameterDirections[15] = ParameterDirection.Input;
                parameterDirections[16] = ParameterDirection.Input;
                parameterDirections[17] = ParameterDirection.Input;
                parameterDirections[18] = ParameterDirection.Input;
                parameterDirections[19] = ParameterDirection.Input;
                parameterDirections[20] = ParameterDirection.Input;
                parameterDirections[21] = ParameterDirection.Input;
                parameterDirections[22] = ParameterDirection.Input;
                parameterDirections[23] = ParameterDirection.Output;
                db.ParameterDirections = parameterDirections;

                object[] parameterValues = new object[24];
                parameterValues[0] = UserName;
                parameterValues[1] = UserCode;
                parameterValues[2] = UserTypeID;
                parameterValues[3] = DeptID;
                parameterValues[4] = DesigID;
                parameterValues[5] = OTLNumber;
                parameterValues[6] = DrivingLicenseNo;
                parameterValues[7] = DrivingLicenseValid;
                parameterValues[8] = ValidityHazEndor;
                parameterValues[9] = Day3PCVOCertNo;
                parameterValues[10] = Day3PCVOCertDate;
                parameterValues[11] = Day3PCVOValid;
                parameterValues[12] = Day1PCVOCertNo;
                parameterValues[13] = Day1PCVOCertDate;
                parameterValues[14] = Day1PCVOValid;
                parameterValues[15] = PoliceVerification;
                parameterValues[16] = FingerOne;
                parameterValues[17] = FingerOneCopy;
                parameterValues[18] = FingerTwo;
                parameterValues[19] = FingerTwoCopy;
                parameterValues[20] = RfidCard;
                parameterValues[21] = UserPhoto;
                parameterValues[22] = CreatedDate;
                parameterValues[23] = DBNull.Value;
                db.ParameterValues = parameterValues;

                db.InsertRecords();
                GetReturnValues(db);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Updatess data to database
        /// <summary>
        public void Updatevirdiuser()
        {
            try
            {
                MySqlDataBaseConnections db = new MySqlDataBaseConnections();
                db.UserQueryType = QueryType.StoredProcedure;
                db.StoredProcedure = "uspUpdatevirdiuser";

                string[] parameterNames = new string[25];
                parameterNames[0] = userIDName;
                parameterNames[1] = userNameName;
                parameterNames[2] = userCodeName;
                parameterNames[3] = userTypeIDName;
                parameterNames[4] = deptIDName;
                parameterNames[5] = desigIDName;
                parameterNames[6] = oTLNumberName;
                parameterNames[7] = drivingLicenseNoName;
                parameterNames[8] = drivingLicenseValidName;
                parameterNames[9] = validityHazEndorName;
                parameterNames[10] = day3PCVOCertNoName;
                parameterNames[11] = day3PCVOCertDateName;
                parameterNames[12] = day3PCVOValidName;
                parameterNames[13] = day1PCVOCertNoName;
                parameterNames[14] = day1PCVOCertDateName;
                parameterNames[15] = day1PCVOValidName;
                parameterNames[16] = policeVerificationName;
                parameterNames[17] = fingerOneName;
                parameterNames[18] = fingerOneCopyName;
                parameterNames[19] = fingerTwoName;
                parameterNames[20] = fingerTwoCopyName;
                parameterNames[21] = rfidCardName;
                parameterNames[22] = userPhotoName;
                parameterNames[23] = createdDateName;
                parameterNames[24] = successName;
                db.ParameterNames = parameterNames;

                MySqlDbType[] parameterTypes = new MySqlDbType[25];
                parameterTypes[0] = userIDType;
                parameterTypes[1] = userNameType;
                parameterTypes[2] = userCodeType;
                parameterTypes[3] = userTypeIDType;
                parameterTypes[4] = deptIDType;
                parameterTypes[5] = desigIDType;
                parameterTypes[6] = oTLNumberType;
                parameterTypes[7] = drivingLicenseNoType;
                parameterTypes[8] = drivingLicenseValidType;
                parameterTypes[9] = validityHazEndorType;
                parameterTypes[10] = day3PCVOCertNoType;
                parameterTypes[11] = day3PCVOCertDateType;
                parameterTypes[12] = day3PCVOValidType;
                parameterTypes[13] = day1PCVOCertNoType;
                parameterTypes[14] = day1PCVOCertDateType;
                parameterTypes[15] = day1PCVOValidType;
                parameterTypes[16] = policeVerificationType;
                parameterTypes[17] = fingerOneType;
                parameterTypes[18] = fingerOneCopyType;
                parameterTypes[19] = fingerTwoType;
                parameterTypes[20] = fingerTwoCopyType;
                parameterTypes[21] = rfidCardType;
                parameterTypes[22] = userPhotoType;
                parameterTypes[23] = createdDateType;
                parameterTypes[24] = successType;
                db.ParameterTypes = parameterTypes;

                ParameterDirection[] parameterDirections = new ParameterDirection[25];
                parameterDirections[0] = ParameterDirection.Input;
                parameterDirections[1] = ParameterDirection.Input;
                parameterDirections[2] = ParameterDirection.Input;
                parameterDirections[3] = ParameterDirection.Input;
                parameterDirections[4] = ParameterDirection.Input;
                parameterDirections[5] = ParameterDirection.Input;
                parameterDirections[6] = ParameterDirection.Input;
                parameterDirections[7] = ParameterDirection.Input;
                parameterDirections[8] = ParameterDirection.Input;
                parameterDirections[9] = ParameterDirection.Input;
                parameterDirections[10] = ParameterDirection.Input;
                parameterDirections[11] = ParameterDirection.Input;
                parameterDirections[12] = ParameterDirection.Input;
                parameterDirections[13] = ParameterDirection.Input;
                parameterDirections[14] = ParameterDirection.Input;
                parameterDirections[15] = ParameterDirection.Input;
                parameterDirections[16] = ParameterDirection.Input;
                parameterDirections[17] = ParameterDirection.Input;
                parameterDirections[18] = ParameterDirection.Input;
                parameterDirections[19] = ParameterDirection.Input;
                parameterDirections[20] = ParameterDirection.Input;
                parameterDirections[21] = ParameterDirection.Input;
                parameterDirections[22] = ParameterDirection.Input;
                parameterDirections[23] = ParameterDirection.Input;
                parameterDirections[24] = ParameterDirection.Output;
                db.ParameterDirections = parameterDirections;

                object[] parameterValues = new object[25];
                parameterValues[0] = UserID;
                parameterValues[1] = UserName;
                parameterValues[2] = UserCode;
                parameterValues[3] = UserTypeID;
                parameterValues[4] = DeptID;
                parameterValues[5] = DesigID;
                parameterValues[6] = OTLNumber;
                parameterValues[7] = DrivingLicenseNo;
                parameterValues[8] = DrivingLicenseValid;
                parameterValues[9] = ValidityHazEndor;
                parameterValues[10] = Day3PCVOCertNo;
                parameterValues[11] = Day3PCVOCertDate;
                parameterValues[12] = Day3PCVOValid;
                parameterValues[13] = Day1PCVOCertNo;
                parameterValues[14] = Day1PCVOCertDate;
                parameterValues[15] = Day1PCVOValid;
                parameterValues[16] = PoliceVerification;
                parameterValues[17] = FingerOne;
                parameterValues[18] = FingerOneCopy;
                parameterValues[19] = FingerTwo;
                parameterValues[20] = FingerTwoCopy;
                parameterValues[21] = RfidCard;
                parameterValues[22] = UserPhoto;
                parameterValues[23] = CreatedDate;
                parameterValues[24] = DBNull.Value;
                db.ParameterValues = parameterValues;

                db.UpdateRecords();
                GetReturnValues(db);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Updatess data to database
        /// <summary>
        public void UpdatevirdiuserEnrolment()
        {
            try
            {
                MySqlDataBaseConnections db = new MySqlDataBaseConnections();
                db.UserQueryType = QueryType.StoredProcedure;
                db.StoredProcedure = "uspUpdatevirdiuserEnrolment";

                string[] parameterNames = new string[7];
                parameterNames[0] = userIDName;
                parameterNames[1] = fingerOneName;
                parameterNames[2] = fingerOneCopyName;
                parameterNames[3] = fingerTwoName;
                parameterNames[4] = fingerTwoCopyName;
                parameterNames[5] = rfidCardName;
                parameterNames[6] = successName;
                db.ParameterNames = parameterNames;

                MySqlDbType[] parameterTypes = new MySqlDbType[7];
                parameterTypes[0] = userIDType;
                parameterTypes[1] = fingerOneType;
                parameterTypes[2] = fingerOneCopyType;
                parameterTypes[3] = fingerTwoType;
                parameterTypes[4] = fingerTwoCopyType;
                parameterTypes[5] = rfidCardType;
                parameterTypes[6] = successType;
                db.ParameterTypes = parameterTypes;

                ParameterDirection[] parameterDirections = new ParameterDirection[7];
                parameterDirections[0] = ParameterDirection.Input;
                parameterDirections[1] = ParameterDirection.Input;
                parameterDirections[2] = ParameterDirection.Input;
                parameterDirections[3] = ParameterDirection.Input;
                parameterDirections[4] = ParameterDirection.Input;
                parameterDirections[5] = ParameterDirection.Input;
                parameterDirections[6] = ParameterDirection.Output;
                db.ParameterDirections = parameterDirections;

                object[] parameterValues = new object[7];
                parameterValues[0] = UserID;
                parameterValues[1] = FingerOne;
                parameterValues[2] = FingerOneCopy;
                parameterValues[3] = FingerTwo;
                parameterValues[4] = FingerTwoCopy;
                parameterValues[5] = RfidCard;
                parameterValues[6] = DBNull.Value;
                db.ParameterValues = parameterValues;

                db.UpdateRecords();
                GetReturnValues(db);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
		
		/// <summary>
		/// Deletes data from database
		/// <summary>
		public void Deletevirdiuser()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspDeletevirdiuser";
		
				string[] parameterNames = new string[1];
				parameterNames[0] = userIDName;
				db.ParameterNames = parameterNames;
		
				MySqlDbType[] parameterTypes = new MySqlDbType[1];
				parameterTypes[0] = userIDType;
				db.ParameterTypes = parameterTypes;
		
				ParameterDirection[] parameterDirections = new ParameterDirection[1];
				parameterDirections[0] = ParameterDirection.Input;
				db.ParameterDirections = parameterDirections;
		
				object[] parameterValues = new object[1];
				parameterValues[0] = UserID;
				db.ParameterValues = parameterValues;
		
				db.DeleteRecords();
				GetReturnValues(db);
		
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Gets the output valus from the stored procedure for the object db
		/// <summary>
		/// <param name="db">SqlDataBaseConnections object</param>
		private void GetReturnValues(MySqlDataBaseConnections db)
		{
			if (db.ReturnValues != null)
			{
				for (int i = 0; i < db.ReturnValues.Length / 2; i++)
				{
					if (((string)db.ReturnValues[i, 0]).CompareTo(successName) == 0)
						msuccess = (int)db.ReturnValues[i, 1];
				}
			}
		}
		
	}
}

