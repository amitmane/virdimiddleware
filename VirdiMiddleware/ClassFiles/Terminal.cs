﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VirdiMiddleware
{
    public class Terminal
    {
        public int TerminalCode{ get; set; }
        public DateTime ConnectionTime{ get; set; }
    }
}
