﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using System.Configuration;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Data;

/// <summary>
/// Summary description for DataBaseConnections
/// </summary>
namespace Com.Rasilant.Database
{
    public class MySqlDataBaseConnections
    {
        /// <summary>
        /// Connection parameters
        /// </summary>
        private string connectionString;
        private MySqlConnection connection;

        /// <summary>
        /// Initializes SqlServer DataBase connections
        /// </summary>
        public MySqlDataBaseConnections()
        {
            connectionString = System.Configuration.ConfigurationSettings.AppSettings["MySqlConnection"].ToString();
            connection = new MySqlConnection();
            connection.ConnectionString = connectionString;
        }
        /// <summary>
        /// Sets MySqlParameter's ParameterNames
        /// </summary>
        private string[] mparameterNames;
        public string[] ParameterNames
        {
            set
            {
                mparameterNames = value;
            }
        }

        /// <summary>
        /// Sets MySqlParameter's SqlDbTypes
        /// </summary>
        private MySqlDbType[] mparameterTypes;
        public MySqlDbType[] ParameterTypes
        {
            get
            {
                return mparameterTypes;
            }
            set
            {
                mparameterTypes = value;
            }
        }

        /// <summary>
        /// Sets MySqlParameter's ParameterDirections
        /// </summary>
        private ParameterDirection[] mparameterDirections;
        public ParameterDirection[] ParameterDirections
        {
            get
            {
                return mparameterDirections;
            }
            set
            {
                mparameterDirections = value;
            }
        }

        /// <summary>
        /// Sets MySqlParameter's ParameterValues
        /// </summary>
        private object[] mparameterValues;
        public object[] ParameterValues
        {
            get
            {
                return mparameterValues;
            }
            set
            {
                mparameterValues = value;
            }
        }

        /// <summary>
        /// Sets StoredProcedure's name
        /// </summary>
        private string mstoredProcedure;
        public string StoredProcedure
        {
            get
            {
                return mstoredProcedure;
            }
            set
            {
                mstoredProcedure = value;
            }
        }

        /// <summary>
        /// Sets Query string
        /// </summary>
        private string mquery;
        public string Query
        {
            set
            {
                mquery = value;
            }
        }

        /// <summary>
        /// Set's QueryType wheter it is Query text or Stored procedure
        /// </summary>
        private QueryType muserQueryType;
        public QueryType UserQueryType
        {
            set
            {
                muserQueryType = value;
            }
        }

        /// <summary>
        /// Get's the return values of sql parameters
        /// </summary>
        private object[,] mreturnValues;
        public object[,] ReturnValues
        {
            get
            {
                return mreturnValues;
            }
        }

        private int mreturnValue;
        public int ReturnValue
        {
            get
            {
                return mreturnValue;
            }
        }

        /// <summary>
        /// Converts MySqlDataReader object to DataSet object
        /// </summary>
        /// <param name="reader">MySqlDataReader's object</param>
        /// <returns>DataSet's object</returns>
        private DataSet DataReaderToDataSet(IDataReader reader)
        {
            DataSet ds = new DataSet();
            DataTable dataTable = new DataTable();

            DataTable schemaTable = reader.GetSchemaTable();

            DataRow row;
            string columnName;
            DataColumn column;

            int count = schemaTable.Rows.Count;

            for (int i = 0; i < count; i++)
            {
                row = schemaTable.Rows[i];
                columnName = (string)row["ColumnName"];
                column = new DataColumn(columnName, (Type)row["DataType"]);
                dataTable.Columns.Add(column);
            }
            ds.Tables.Add(dataTable);

            object[] values = new object[count];

            try
            {
                dataTable.BeginLoadData();
                while (reader.Read())
                {
                    reader.GetValues(values);
                    dataTable.LoadDataRow(values, true);
                }

            }
            finally
            {
                dataTable.EndLoadData();
                reader.Close();
            }

            return ds;
        }

        /// <summary>
        /// Returns data from the database
        /// </summary>
        /// <returns>DataSet's object</returns>
        public DataSet GetRecords()
        {
            try
            {
                DataSet ds = new DataSet();
                MySqlCommand command = new MySqlCommand();
                if (muserQueryType == QueryType.StoredProcedure)
                {
                    command.CommandText = mstoredProcedure;
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 0;
                    if (mparameterNames != null)
                    {
                        MySqlParameter[] parameters = new MySqlParameter[mparameterNames.Length];
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            parameters[i] = new MySqlParameter();
                            parameters[i].ParameterName = mparameterNames[i];
                            parameters[i].MySqlDbType = mparameterTypes[i];
                            if (mparameterDirections[i] == ParameterDirection.Output)
                            {
                                parameters[i].Direction = mparameterDirections[i];
                                parameters[i].SourceColumn = mparameterNames[i].Substring(2);
                            }
                            else
                            {
                                parameters[i].Value = mparameterValues[i];
                            }
                            command.Parameters.Add(parameters[i]);
                        }
                    }
                }
                else
                {
                    command.CommandText = mquery;
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                }
                connection.Open();
                if (connection.State == ConnectionState.Open)
                {
                    MySqlDataReader reader = command.ExecuteReader();
                    if (reader != null)
                    {
                        ds = DataReaderToDataSet(reader);
                        reader.Close();
                        connection.Close();
                    }
                    if (connection.State != ConnectionState.Closed)
                    {
                        throw new Exception(connection.State.ToString());
                    }
                    /*else
                        throw new Exception(connection.State.ToString());*/
                }
                else
                    throw new Exception(connection.State.ToString());
                if (ds != null)
                    return ds;
                else
                    throw new Exception("DataSet not filled");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        /// <summary>
        /// Inserts data to the database
        /// </summary>
        /// <returns>DataSet's object</returns>
        public void InsertRecords()
        {
            try
            {
                MySqlCommand command = new MySqlCommand();
                if (muserQueryType == QueryType.StoredProcedure)
                {
                    command.CommandText = mstoredProcedure;
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;

                    MySqlParameter[] parameters = new MySqlParameter[mparameterNames.Length];
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        parameters[i] = new MySqlParameter();
                        parameters[i].ParameterName = mparameterNames[i];
                        parameters[i].MySqlDbType = mparameterTypes[i];
                        if (mparameterDirections[i] == ParameterDirection.Output)
                        {
                            parameters[i].Direction = mparameterDirections[i];
                            parameters[i].SourceColumn = mparameterNames[i].Substring(2);
                        }
                        else
                        {
                            parameters[i].Value = mparameterValues[i];
                        }
                        command.Parameters.Add(parameters[i]);
                    }
                }
                else
                {
                    command.CommandText = mquery;
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                }
                connection.Open();
                if (connection.State == ConnectionState.Open)
                {
                    mreturnValue = command.ExecuteNonQuery();
                    if (muserQueryType == QueryType.StoredProcedure)
                    {
                        int count = 0;
                        for (int i = 0; i < mparameterNames.Length; i++)
                            if (mparameterDirections[i] == ParameterDirection.Output)
                                count++;
                        if (count > 0)
                        {
                            mreturnValues = null;
                            mreturnValues = new object[count, 2];
                            for (int i = 0, j = 0; i < mparameterNames.Length && j < count; i++)
                                if (mparameterDirections[i] == ParameterDirection.Output)
                                {
                                    mreturnValues[j, 0] = mparameterNames[i];
                                    mreturnValues[j, 1] = command.Parameters[i].Value;
                                    j++;
                                }
                        }
                    }
                    connection.Close();
                    if (connection.State != ConnectionState.Closed)
                    {
                        throw new Exception(connection.State.ToString());
                    }
                }
                else
                    throw new Exception(connection.State.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        /// <summary>
        /// Updates data to the database
        /// </summary>
        /// <returns>DataSet's object</returns>
        public void UpdateRecords()
        {
            try
            {
                MySqlCommand command = new MySqlCommand();
                if (muserQueryType == QueryType.StoredProcedure)
                {
                    command.CommandText = mstoredProcedure;
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;

                    MySqlParameter[] parameters = new MySqlParameter[mparameterNames.Length];
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        parameters[i] = new MySqlParameter();
                        parameters[i].ParameterName = mparameterNames[i];
                        parameters[i].MySqlDbType = mparameterTypes[i];
                        if (mparameterDirections[i] == ParameterDirection.Output)
                        {
                            parameters[i].Direction = mparameterDirections[i];
                            parameters[i].SourceColumn = mparameterNames[i].Substring(2);
                        }
                        else
                        {
                            parameters[i].Value = mparameterValues[i];
                        }
                        command.Parameters.Add(parameters[i]);
                    }
                }
                else
                {
                    command.CommandText = mquery;
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                }
                connection.Open();
                if (connection.State == ConnectionState.Open)
                {
                    mreturnValue = command.ExecuteNonQuery();
                    if (muserQueryType == QueryType.StoredProcedure)
                    {
                        int count = 0;
                        for (int i = 0; i < mparameterNames.Length; i++)
                            if (mparameterDirections[i] == ParameterDirection.Output)
                                count++;
                        if (count > 0)
                        {
                            mreturnValues = null;
                            mreturnValues = new object[count, 2];
                            for (int i = 0, j = 0; i < mparameterNames.Length && j < count; i++)
                                if (mparameterDirections[i] == ParameterDirection.Output)
                                {
                                    mreturnValues[j, 0] = mparameterNames[i];
                                    mreturnValues[j, 1] = command.Parameters[i].Value;
                                    j++;
                                }
                        }
                    }
                    connection.Close();
                    if (connection.State != ConnectionState.Closed)
                    {
                        throw new Exception(connection.State.ToString());
                    }
                }
                else
                    throw new Exception(connection.State.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        /// <summary>
        /// Deletes data from the database
        /// </summary>
        /// <returns>DataSet's object</returns>
        public void DeleteRecords()
        {
            try
            {
                MySqlCommand command = new MySqlCommand();
                if (muserQueryType == QueryType.StoredProcedure)
                {
                    command.CommandText = mstoredProcedure;
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;

                    MySqlParameter[] parameters = new MySqlParameter[mparameterNames.Length];
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        parameters[i] = new MySqlParameter();
                        parameters[i].ParameterName = mparameterNames[i];
                        parameters[i].MySqlDbType = mparameterTypes[i];
                        if (mparameterDirections[i] == ParameterDirection.Output)
                        {
                            parameters[i].Direction = mparameterDirections[i];
                            parameters[i].SourceColumn = mparameterNames[i].Substring(2);
                        }
                        else
                        {
                            parameters[i].Value = mparameterValues[i];
                        }
                        command.Parameters.Add(parameters[i]);
                    }
                }
                else
                {
                    command.CommandText = mquery;
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                }
                connection.Open();
                if (connection.State == ConnectionState.Open)
                {
                    mreturnValue = command.ExecuteNonQuery();
                    if (muserQueryType == QueryType.StoredProcedure)
                    {
                        int count = 0;
                        for (int i = 0; i < mparameterNames.Length; i++)
                            if (mparameterDirections[i] == ParameterDirection.Output)
                                count++;
                        if (count > 0)
                        {
                            mreturnValues = null;
                            mreturnValues = new object[count, 2];
                            for (int i = 0, j = 0; i < mparameterNames.Length && j < count; i++)
                                if (mparameterDirections[i] == ParameterDirection.Output)
                                {
                                    mreturnValues[j, 0] = mparameterNames[i];
                                    mreturnValues[j, 1] = command.Parameters[i].Value;
                                    j++;
                                }
                        }
                    }
                    connection.Close();
                    if (connection.State != ConnectionState.Closed)
                    {
                        throw new Exception(connection.State.ToString());
                    }
                }
                else
                    throw new Exception(connection.State.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        /// <summary>
        /// Returns data from the database
        /// </summary>
        /// <returns>DataSet's object</returns>
        public DataSet GetRecordsWithPaging(int startRecords, int maxRecords)
        {
            try
            {
                DataSet ds = new DataSet();
                MySqlCommand command = new MySqlCommand();
                if (muserQueryType == QueryType.StoredProcedure)
                {
                    command.CommandText = mstoredProcedure;
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;

                    if (mparameterNames != null)
                    {
                        MySqlParameter[] parameters = new MySqlParameter[mparameterNames.Length];
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            parameters[i] = new MySqlParameter();
                            parameters[i].ParameterName = mparameterNames[i];
                            parameters[i].MySqlDbType = mparameterTypes[i];
                            if (mparameterDirections[i] == ParameterDirection.Output)
                            {
                                parameters[i].Direction = mparameterDirections[i];
                                parameters[i].SourceColumn = mparameterNames[i].Substring(2);
                            }
                            else
                            {
                                parameters[i].Value = mparameterValues[i];
                            }
                            command.Parameters.Add(parameters[i]);
                        }
                    }
                }
                else
                {
                    command.CommandText = mquery;
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                }
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                adapter.SelectCommand = command;
                DataSet temp = new DataSet();
                adapter.Fill(temp);
                adapter.Fill(ds, startRecords, maxRecords, temp.Tables[0].TableName);
                temp = null;
                if (ds != null)
                    return ds;
                else
                    throw new Exception("DataSet not filled");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
    }
}