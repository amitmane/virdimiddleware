using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using Com.Rasilant.Database;

namespace Com.Rasilant.Services
{
	
	public class bizterminalmessages
	{
		/// <summary>
		/// Parameter name variables
		/// <summary>
		private string messageIDName;
		private string terminalIDName;
		private string userIDName;
		private string messageDateTimeName;
		private string authModeName;
		private string authTypeName;
		private string isAuthorizedName;
		private string deviceIDName;
		private string authResultName;
		private string RFIDName;
		private string PictureDataName;
		private string CurrentTimeName;
		private string ErrorCodeName;
		private string successName;
		
		/// <summary>
		/// Parameter type variables
		/// <summary>
		private MySqlDbType messageIDType;
		private MySqlDbType terminalIDType;
		private MySqlDbType userIDType;
		private MySqlDbType messageDateTimeType;
		private MySqlDbType authModeType;
		private MySqlDbType authTypeType;
		private MySqlDbType isAuthorizedType;
		private MySqlDbType deviceIDType;
		private MySqlDbType authResultType;
		private MySqlDbType RFIDType;
		private MySqlDbType PictureDataType;
		private MySqlDbType CurrentTimeType;
		private MySqlDbType ErrorCodeType;
		private MySqlDbType successType;
		
		/// <summary>
		/// terminalmessages constructor
		/// <summary>
		public bizterminalmessages()
		{
			//
			// Parameter name variables initializations
			//
			messageIDName = "P_messageID";
			terminalIDName = "P_terminalID";
			userIDName = "P_userID";
			messageDateTimeName = "P_messageDateTime";
			authModeName = "P_authMode";
			authTypeName = "P_authType";
			isAuthorizedName = "P_isAuthorized";
			deviceIDName = "P_deviceID";
			authResultName = "P_authResult";
			RFIDName = "P_RFID";
			PictureDataName = "P_PictureData";
			CurrentTimeName = "P_CurrentTime";
			ErrorCodeName = "P_ErrorCode";
			successName = "P_success";
			
			//
			// Parameter type variables initialization
			//
			messageIDType = MySqlDbType.Int32;
			terminalIDType = MySqlDbType.Int32;
			userIDType = MySqlDbType.Float;
			messageDateTimeType = MySqlDbType.DateTime;
			authModeType = MySqlDbType.Int32;
			authTypeType = MySqlDbType.Int32;
			isAuthorizedType = MySqlDbType.Int32;
			deviceIDType = MySqlDbType.Int32;
			authResultType = MySqlDbType.Int32;
			RFIDType = MySqlDbType.VarChar;
            PictureDataType = MySqlDbType.Blob;
			CurrentTimeType = MySqlDbType.DateTime;
			ErrorCodeType = MySqlDbType.Int32;
			successType = MySqlDbType.Int32;
		}
		
		/// <summary>
		/// Gets or sets MessageID
		/// <summary>
		private object mmessageID;
		public object MessageID
		{
			get
			{
				return mmessageID;
			}
			set
			{
				mmessageID = value;
			}
		}
		
		/// <summary>
		/// Gets or sets TerminalID
		/// <summary>
		private object mterminalID;
		public object TerminalID
		{
			get
			{
				return mterminalID;
			}
			set
			{
				mterminalID = value;
			}
		}
		
		/// <summary>
		/// Gets or sets UserID
		/// <summary>
		private object muserID;
		public object UserID
		{
			get
			{
				return muserID;
			}
			set
			{
				muserID = value;
			}
		}
		
		/// <summary>
		/// Gets or sets MessageDateTime
		/// <summary>
		private object mmessageDateTime;
		public object MessageDateTime
		{
			get
			{
				return mmessageDateTime;
			}
			set
			{
				mmessageDateTime = value;
			}
		}
		
		/// <summary>
		/// Gets or sets AuthMode
		/// <summary>
		private object mauthMode;
		public object AuthMode
		{
			get
			{
				return mauthMode;
			}
			set
			{
				mauthMode = value;
			}
		}
		
		/// <summary>
		/// Gets or sets AuthType
		/// <summary>
		private object mauthType;
		public object AuthType
		{
			get
			{
				return mauthType;
			}
			set
			{
				mauthType = value;
			}
		}
		
		/// <summary>
		/// Gets or sets IsAuthorized
		/// <summary>
		private object misAuthorized;
		public object IsAuthorized
		{
			get
			{
				return misAuthorized;
			}
			set
			{
				misAuthorized = value;
			}
		}
		
		/// <summary>
		/// Gets or sets DeviceID
		/// <summary>
		private object mdeviceID;
		public object DeviceID
		{
			get
			{
				return mdeviceID;
			}
			set
			{
				mdeviceID = value;
			}
		}
		
		/// <summary>
		/// Gets or sets AuthResult
		/// <summary>
		private object mauthResult;
		public object AuthResult
		{
			get
			{
				return mauthResult;
			}
			set
			{
				mauthResult = value;
			}
		}
		
		/// <summary>
		/// Gets or sets RFID
		/// <summary>
		private object mRFID;
		public object RFID
		{
			get
			{
				return mRFID;
			}
			set
			{
				mRFID = value;
			}
		}
		
		/// <summary>
		/// Gets or sets PictureData
		/// <summary>
		private object mPictureData;
		public object PictureData
		{
			get
			{
				return mPictureData;
			}
			set
			{
				mPictureData = value;
			}
		}
		
		/// <summary>
		/// Gets or sets CurrentTime
		/// <summary>
		private object mCurrentTime;
		public object CurrentTime
		{
			get
			{
				return mCurrentTime;
			}
			set
			{
				mCurrentTime = value;
			}
		}
		
		/// <summary>
		/// Gets or sets ErrorCode
		/// <summary>
		private object mErrorCode;
		public object ErrorCode
		{
			get
			{
				return mErrorCode;
			}
			set
			{
				mErrorCode = value;
			}
		}
		
		/// <summary>
		/// Gets or sets result for insert or update records to database.
		/// <summary>
		private int msuccess;
		public int Success
		{
			get
			{
				return msuccess;
			}
			set
			{
				msuccess = value;
			}
		}
		
		/// <summary>
		/// Gets or sets terminalmessages table
		/// <summary>
		private DataSet mdsterminalmessages;
		public DataSet Dsterminalmessages
		{
			get
			{
				return mdsterminalmessages;
			}
			set
			{
				mdsterminalmessages = value;
			}
		}
		
		/// <summary>
		/// Retrives data from database
		/// <summary>
		public void Searchterminalmessages()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspSearchterminalmessages";
				Dsterminalmessages = db.GetRecords();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Retrives data from database
		/// <summary>
		public void SearchterminalmessagesByID()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspSearchterminalmessagesByID";
		
				string[] parameterNames = new string[1];
				parameterNames[0] = messageIDName;
				db.ParameterNames = parameterNames;
		
				MySqlDbType[] parameterTypes = new MySqlDbType[1];
				parameterTypes[0] = messageIDType;
				db.ParameterTypes = parameterTypes;
		
				ParameterDirection[] parameterDirections = new ParameterDirection[1];
				parameterDirections[0] = ParameterDirection.Input;
				db.ParameterDirections = parameterDirections;
		
				object[] parameterValues = new object[1];
				parameterValues[0] = MessageID;
				db.ParameterValues = parameterValues;
		
				Dsterminalmessages = db.GetRecords();
		
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Inserts data to database
		/// <summary>
		public void Insertterminalmessages()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspInsertterminalmessages";
		
				string[] parameterNames = new string[13];
				parameterNames[0] = terminalIDName;
				parameterNames[1] = userIDName;
				parameterNames[2] = messageDateTimeName;
				parameterNames[3] = authModeName;
				parameterNames[4] = authTypeName;
				parameterNames[5] = isAuthorizedName;
				parameterNames[6] = deviceIDName;
				parameterNames[7] = authResultName;
				parameterNames[8] = RFIDName;
				parameterNames[9] = PictureDataName;
				parameterNames[10] = CurrentTimeName;
				parameterNames[11] = ErrorCodeName;
				parameterNames[12] = successName;
				db.ParameterNames = parameterNames;
		
				MySqlDbType[] parameterTypes = new MySqlDbType[13];
				parameterTypes[0] = terminalIDType;
				parameterTypes[1] = userIDType;
				parameterTypes[2] = messageDateTimeType;
				parameterTypes[3] = authModeType;
				parameterTypes[4] = authTypeType;
				parameterTypes[5] = isAuthorizedType;
				parameterTypes[6] = deviceIDType;
				parameterTypes[7] = authResultType;
				parameterTypes[8] = RFIDType;
				parameterTypes[9] = PictureDataType;
				parameterTypes[10] = CurrentTimeType;
				parameterTypes[11] = ErrorCodeType;
				parameterTypes[12] = successType;
				db.ParameterTypes = parameterTypes;
		
				ParameterDirection[] parameterDirections = new ParameterDirection[13];
				parameterDirections[0] = ParameterDirection.Input;
				parameterDirections[1] = ParameterDirection.Input;
				parameterDirections[2] = ParameterDirection.Input;
				parameterDirections[3] = ParameterDirection.Input;
				parameterDirections[4] = ParameterDirection.Input;
				parameterDirections[5] = ParameterDirection.Input;
				parameterDirections[6] = ParameterDirection.Input;
				parameterDirections[7] = ParameterDirection.Input;
				parameterDirections[8] = ParameterDirection.Input;
				parameterDirections[9] = ParameterDirection.Input;
				parameterDirections[10] = ParameterDirection.Input;
				parameterDirections[11] = ParameterDirection.Input;
				parameterDirections[12] = ParameterDirection.Output;
				db.ParameterDirections = parameterDirections;
		
				object[] parameterValues = new object[13];
				parameterValues[0] = TerminalID;
				parameterValues[1] = UserID;
				parameterValues[2] = MessageDateTime;
				parameterValues[3] = AuthMode;
				parameterValues[4] = AuthType;
				parameterValues[5] = IsAuthorized;
				parameterValues[6] = DeviceID;
				parameterValues[7] = AuthResult;
				parameterValues[8] = RFID;
				parameterValues[9] = PictureData;
				parameterValues[10] = CurrentTime;
				parameterValues[11] = ErrorCode;
				parameterValues[12] = DBNull.Value;
				db.ParameterValues = parameterValues;
		
				db.InsertRecords();
				GetReturnValues(db);
		
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Updatess data to database
		/// <summary>
		public void Updateterminalmessages()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspUpdateterminalmessages";
		
				string[] parameterNames = new string[14];
				parameterNames[0] = messageIDName;
				parameterNames[1] = terminalIDName;
				parameterNames[2] = userIDName;
				parameterNames[3] = messageDateTimeName;
				parameterNames[4] = authModeName;
				parameterNames[5] = authTypeName;
				parameterNames[6] = isAuthorizedName;
				parameterNames[7] = deviceIDName;
				parameterNames[8] = authResultName;
				parameterNames[9] = RFIDName;
				parameterNames[10] = PictureDataName;
				parameterNames[11] = CurrentTimeName;
				parameterNames[12] = ErrorCodeName;
				parameterNames[13] = successName;
				db.ParameterNames = parameterNames;
		
				MySqlDbType[] parameterTypes = new MySqlDbType[14];
				parameterTypes[0] = messageIDType;
				parameterTypes[1] = terminalIDType;
				parameterTypes[2] = userIDType;
				parameterTypes[3] = messageDateTimeType;
				parameterTypes[4] = authModeType;
				parameterTypes[5] = authTypeType;
				parameterTypes[6] = isAuthorizedType;
				parameterTypes[7] = deviceIDType;
				parameterTypes[8] = authResultType;
				parameterTypes[9] = RFIDType;
				parameterTypes[10] = PictureDataType;
				parameterTypes[11] = CurrentTimeType;
				parameterTypes[12] = ErrorCodeType;
				parameterTypes[13] = successType;
				db.ParameterTypes = parameterTypes;
		
				ParameterDirection[] parameterDirections = new ParameterDirection[14];
				parameterDirections[0] = ParameterDirection.Input;
				parameterDirections[1] = ParameterDirection.Input;
				parameterDirections[2] = ParameterDirection.Input;
				parameterDirections[3] = ParameterDirection.Input;
				parameterDirections[4] = ParameterDirection.Input;
				parameterDirections[5] = ParameterDirection.Input;
				parameterDirections[6] = ParameterDirection.Input;
				parameterDirections[7] = ParameterDirection.Input;
				parameterDirections[8] = ParameterDirection.Input;
				parameterDirections[9] = ParameterDirection.Input;
				parameterDirections[10] = ParameterDirection.Input;
				parameterDirections[11] = ParameterDirection.Input;
				parameterDirections[12] = ParameterDirection.Input;
				parameterDirections[13] = ParameterDirection.Output;
				db.ParameterDirections = parameterDirections;
		
				object[] parameterValues = new object[14];
				parameterValues[0] = MessageID;
				parameterValues[1] = TerminalID;
				parameterValues[2] = UserID;
				parameterValues[3] = MessageDateTime;
				parameterValues[4] = AuthMode;
				parameterValues[5] = AuthType;
				parameterValues[6] = IsAuthorized;
				parameterValues[7] = DeviceID;
				parameterValues[8] = AuthResult;
				parameterValues[9] = RFID;
				parameterValues[10] = PictureData;
				parameterValues[11] = CurrentTime;
				parameterValues[12] = ErrorCode;
				parameterValues[13] = DBNull.Value;
				db.ParameterValues = parameterValues;
		
				db.UpdateRecords();
				GetReturnValues(db);
		
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Deletes data from database
		/// <summary>
		public void Deleteterminalmessages()
		{
			try
			{
				MySqlDataBaseConnections db = new MySqlDataBaseConnections();
				db.UserQueryType = QueryType.StoredProcedure;
				db.StoredProcedure = "uspDeleteterminalmessages";
		
				string[] parameterNames = new string[1];
				parameterNames[0] = messageIDName;
				db.ParameterNames = parameterNames;
		
				MySqlDbType[] parameterTypes = new MySqlDbType[1];
				parameterTypes[0] = messageIDType;
				db.ParameterTypes = parameterTypes;
		
				ParameterDirection[] parameterDirections = new ParameterDirection[1];
				parameterDirections[0] = ParameterDirection.Input;
				db.ParameterDirections = parameterDirections;
		
				object[] parameterValues = new object[1];
				parameterValues[0] = MessageID;
				db.ParameterValues = parameterValues;
		
				db.DeleteRecords();
				GetReturnValues(db);
		
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Gets the output valus from the stored procedure for the object db
		/// <summary>
		/// <param name="db">SqlDataBaseConnections object</param>
		private void GetReturnValues(MySqlDataBaseConnections db)
		{
			if (db.ReturnValues != null)
			{
				for (int i = 0; i < db.ReturnValues.Length / 2; i++)
				{
					if (((string)db.ReturnValues[i, 0]).CompareTo(successName) == 0)
						msuccess = (int)db.ReturnValues[i, 1];
				}
			}
		}
		
	}
}

