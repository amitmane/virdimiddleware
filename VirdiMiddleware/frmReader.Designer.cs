﻿namespace VirdiMiddleware
{
    partial class frmReader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgReder = new System.Windows.Forms.DataGridView();
            this.clmReaderID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmReaderName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmMAC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmIP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlReader = new System.Windows.Forms.Panel();
            this.lblHostIP = new System.Windows.Forms.Label();
            this.lblIPAdd = new System.Windows.Forms.Label();
            this.lblContrMAC = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtIP = new System.Windows.Forms.TextBox();
            this.lblContrID = new System.Windows.Forms.Label();
            this.txtMAC = new System.Windows.Forms.TextBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgReder)).BeginInit();
            this.pnlReader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // dgReder
            // 
            this.dgReder.AllowUserToAddRows = false;
            this.dgReder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgReder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmReaderID,
            this.clmReaderName,
            this.clmMAC,
            this.clmIP});
            this.dgReder.Location = new System.Drawing.Point(12, 12);
            this.dgReder.MultiSelect = false;
            this.dgReder.Name = "dgReder";
            this.dgReder.ReadOnly = true;
            this.dgReder.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgReder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgReder.Size = new System.Drawing.Size(630, 144);
            this.dgReder.TabIndex = 2;
            this.dgReder.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgReder_CellClick);
            // 
            // clmReaderID
            // 
            this.clmReaderID.DataPropertyName = "readerID";
            this.clmReaderID.HeaderText = "ReaderID";
            this.clmReaderID.Name = "clmReaderID";
            this.clmReaderID.ReadOnly = true;
            this.clmReaderID.Visible = false;
            // 
            // clmReaderName
            // 
            this.clmReaderName.DataPropertyName = "readerName";
            this.clmReaderName.HeaderText = "Name";
            this.clmReaderName.Name = "clmReaderName";
            this.clmReaderName.ReadOnly = true;
            this.clmReaderName.Width = 150;
            // 
            // clmMAC
            // 
            this.clmMAC.DataPropertyName = "readerMAC";
            this.clmMAC.HeaderText = "MAC";
            this.clmMAC.Name = "clmMAC";
            this.clmMAC.ReadOnly = true;
            this.clmMAC.Width = 150;
            // 
            // clmIP
            // 
            this.clmIP.DataPropertyName = "readerIP";
            this.clmIP.HeaderText = "IP Address";
            this.clmIP.Name = "clmIP";
            this.clmIP.ReadOnly = true;
            this.clmIP.Width = 150;
            // 
            // pnlReader
            // 
            this.pnlReader.Controls.Add(this.lblHostIP);
            this.pnlReader.Controls.Add(this.lblIPAdd);
            this.pnlReader.Controls.Add(this.lblContrMAC);
            this.pnlReader.Controls.Add(this.txtName);
            this.pnlReader.Controls.Add(this.txtIP);
            this.pnlReader.Controls.Add(this.lblContrID);
            this.pnlReader.Controls.Add(this.txtMAC);
            this.pnlReader.Location = new System.Drawing.Point(12, 178);
            this.pnlReader.Name = "pnlReader";
            this.pnlReader.Size = new System.Drawing.Size(630, 107);
            this.pnlReader.TabIndex = 3;
            // 
            // lblHostIP
            // 
            this.lblHostIP.AutoSize = true;
            this.lblHostIP.Location = new System.Drawing.Point(234, 19);
            this.lblHostIP.Name = "lblHostIP";
            this.lblHostIP.Size = new System.Drawing.Size(41, 13);
            this.lblHostIP.TabIndex = 6;
            this.lblHostIP.Text = "Name :";
            // 
            // lblIPAdd
            // 
            this.lblIPAdd.AutoSize = true;
            this.lblIPAdd.Location = new System.Drawing.Point(214, 71);
            this.lblIPAdd.Name = "lblIPAdd";
            this.lblIPAdd.Size = new System.Drawing.Size(61, 13);
            this.lblIPAdd.TabIndex = 5;
            this.lblIPAdd.Text = "IP Address:";
            // 
            // lblContrMAC
            // 
            this.lblContrMAC.AutoSize = true;
            this.lblContrMAC.Location = new System.Drawing.Point(239, 46);
            this.lblContrMAC.Name = "lblContrMAC";
            this.lblContrMAC.Size = new System.Drawing.Size(36, 13);
            this.lblContrMAC.TabIndex = 4;
            this.lblContrMAC.Text = "MAC :";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(281, 16);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(121, 20);
            this.txtName.TabIndex = 2;
            // 
            // txtIP
            // 
            this.txtIP.Location = new System.Drawing.Point(281, 68);
            this.txtIP.Name = "txtIP";
            this.txtIP.Size = new System.Drawing.Size(121, 20);
            this.txtIP.TabIndex = 1;
            // 
            // lblContrID
            // 
            this.lblContrID.AutoSize = true;
            this.lblContrID.Location = new System.Drawing.Point(327, 45);
            this.lblContrID.Name = "lblContrID";
            this.lblContrID.Size = new System.Drawing.Size(0, 13);
            this.lblContrID.TabIndex = 14;
            this.lblContrID.Visible = false;
            // 
            // txtMAC
            // 
            this.txtMAC.Location = new System.Drawing.Point(281, 42);
            this.txtMAC.Name = "txtMAC";
            this.txtMAC.Size = new System.Drawing.Size(121, 20);
            this.txtMAC.TabIndex = 0;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(430, 303);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 17;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(329, 303);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 16;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(228, 303);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(127, 303);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 23);
            this.btnNew.TabIndex = 14;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // frmReader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 350);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.pnlReader);
            this.Controls.Add(this.dgReder);
            this.Name = "frmReader";
            this.Text = "Reader";
            ((System.ComponentModel.ISupportInitialize)(this.dgReder)).EndInit();
            this.pnlReader.ResumeLayout(false);
            this.pnlReader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgReder;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmReaderID;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmReaderName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmMAC;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmIP;
        private System.Windows.Forms.Panel pnlReader;
        private System.Windows.Forms.Label lblHostIP;
        private System.Windows.Forms.Label lblIPAdd;
        private System.Windows.Forms.Label lblContrMAC;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtIP;
        private System.Windows.Forms.Label lblContrID;
        private System.Windows.Forms.TextBox txtMAC;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}