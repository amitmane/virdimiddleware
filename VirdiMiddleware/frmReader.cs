﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Com.Rasilant.Services;

namespace VirdiMiddleware
{
    public partial class frmReader : Form
    {
        public frmReader()
        {
            InitializeComponent();
        }



        private void dgReder_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    //LoadData(Convert.ToInt32(dgController.Rows[e.RowIndex].Cells[0].Value));
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            btnCancel.Enabled = true;
            btnNew.Enabled = false;
            btnSave.Enabled = false;
            btnDelete.Enabled = false;
            pnlReader.Enabled = true;

            txtName.Text = "";
            txtIP.Text = "";
            txtMAC.Text = "";
            lblContrID.Text = "";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                errorProvider1.SetError(txtIP, "");
                errorProvider1.SetError(txtName, "");
                errorProvider1.SetError(txtMAC, "");
                lblContrID.Text = "";
                pnlReader.Enabled = false;

                btnNew.Enabled = true;
                btnDelete.Enabled = true;

                btnSave.Enabled = false;
                btnCancel.Enabled = false;

                //LoadData(Convert.ToInt32(dgController.Rows[0].Cells[0].Value));
            }
            catch (Exception ex)
            {
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to delete this Reader?", "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //if (lblContrID.Text != "")
                    //{
                    //    bizreader objReader = new bizreader();
                    //    objReader.ReaderID = Convert.ToInt32(lblContrID.Text);
                    //    objReader.Deletereader();

                        
                    //    MessageBox.Show("Reader Successfully Deleted.");
                    //    lblContrID.Text = "";
                    //    FillDataGrid();
                    //    pnlReader.Enabled = false;

                    //    if (dgReder.Rows.Count > 0)
                    //        LoadData(Convert.ToInt32(dgController.Rows[0].Cells[0].Value));
                    //    else
                    //    {
                    //        txtHostIP.Text = "";
                    //        txtIP.Text = "";
                    //        txtMAC.Text = "";
                    //        lblContrID.Text = "";
                    //    }
                    //}
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            bool AddFlag = true;
            if (txtMAC.Text == "")
            {
                AddFlag = false;
                errorProvider1.SetError(txtMAC, "Enter MAC.");
            }
            if (txtIP.Text == "")
            {
                AddFlag = false;
                errorProvider1.SetError(txtIP, "Enter Reader IP.");
            }
            if (txtName.Text == "")
            {
                AddFlag = false;
                errorProvider1.SetError(txtName, "Enter Reader Name.");
            }
            
            if (AddFlag)
            {
                btnNew.Enabled = true;
                btnDelete.Enabled = true;

                btnSave.Enabled = false;
                btnCancel.Enabled = false;

                //InsertUpdateController();
                lblContrID.Text = "";
            }
        }
    }
}
